<?php
/**
 * @link http://www.photosafe.com/
 * @copyright Copyright (c) 2014 PhotoSafe
 * @license http://www.photosafe.com/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Buba
 * @since 1.0
 */
class IE9Asset extends AssetBundle
{
    public function init()
    {
        $this->jsOptions = ['condition' => 'lt IE 9','position' => \yii\web\View::POS_HEAD];
        $this->js        = [
            'js/html5shiv.js',
            'js/respond.min.js',
        ];
        parent::init();
    }
}
