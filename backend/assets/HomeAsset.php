<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class HomeAsset extends AssetBundle
{
    public function init()
    {
        $this->basePath = '@webroot';
        $this->baseUrl  = '@web';
        $this->css      = [
            'css/bootstrap/bootstrap.min.css',
            'css/libs/font-awesome.css',
            'css/libs/nanoscroller.css',
            'css/compiled/theme_styles.css',

            'css/libs/fullcalendar.css',
            'css/libs/fullcalendar.print.css',
            'css/compiled/calendar.css',
            'css/libs/morris.css',
            'css/libs/daterangepicker.css',
            'css/libs/jquery-jvectormap-1.2.2.css',
            'css/libs/nifty-component.css',
            '//fonts.googleapis.com/css?family=Open+Sans:400,600,700,300|Titillium+Web:200,300,400',
        ];
        $this->js       = [
            'js/demo-rtl.js',
            'js/demo-skin-changer.js',
            'js/jquery.js',
            'js/bootstrap.js',
            'js/jquery.nanoscroller.min.js',
            'js/demo.js',

            'js/jquery-ui.custom.min.js',
            'js/fullcalendar.min.js',
            'js/jquery.slimscroll.min.js',
            'js/raphael-min.js',
            'js/morris.min.js',
            'js/moment.min.js',
            'js/daterangepicker.js',
            'js/jquery-jvectormap-1.2.2.min.js',
            'js/jquery-jvectormap-world-merc-en.js',
            'js/gdp-data.js',
            'js/flot/jquery.flot.js',
            'js/flot/jquery.flot.min.js',
            'js/flot/jquery.flot.pie.min.js',
            'js/flot/jquery.flot.stack.min.js',
            'js/flot/jquery.flot.resize.min.js',
            'js/flot/jquery.flot.time.min.js',
            'js/flot/jquery.flot.threshold.js',
            'js/jquery.countTo.js',

            'js/modernizr.custom.js',
            'js/classie.js',
            'js/scripts.js',
            'js/pace.min.js',
        ];
        parent::init();
    }
}
