<?php
/**
 * Created by PhpStorm.
 * User: buba
 * Date: 09.04.15
 * Time: 19:32
 */

namespace backend\models;


class User extends \common\models\User{

    /**
     * @inheritdoc
     */
    public static function find()
    {
        return parent::find()->andWhere(['role'=>[self::ROLE_MODERATOR,self::ROLE_ADMIN]])->one();
    }


}