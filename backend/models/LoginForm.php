<?php
namespace backend\models;

class LoginForm extends \common\models\LoginForm{

    /**
     * Finds user by [[Email]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::find()->where(['email'=>$this->email]);
        }
        return $this->_user;
    }

}