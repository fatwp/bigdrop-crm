<header class="navbar" id="header-navbar">
    <div class="container">
        <a href="/admin" id="logo" class="navbar-brand">
            <img src="/images/logo.png" alt="" class="normal-logo logo-white"/>
            <img src="/images/logo.png" alt="" class="normal-logo logo-black"/>
            <img src="/images/logo.png" alt="" class="small-logo hidden-xs hidden-sm hidden"/>
        </a>

        <div class="clearfix">
            <button class="navbar-toggle" data-target=".navbar-ex1-collapse" data-toggle="collapse" type="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="fa fa-bars"></span>
            </button>

            <div class="nav-no-collapse navbar-left pull-left hidden-sm hidden-xs">
                <ul class="nav navbar-nav pull-left">
                    <li>
                        <a class="btn" id="make-small-nav">
                            <i class="fa fa-bars"></i>
                        </a>
                    </li>
                </ul>
            </div>

            <div class="nav-no-collapse pull-right" id="header-nav">
                <ul class="nav navbar-nav pull-right">

                    <li class="dropdown hidden-xs">
                        <a class="btn dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-envelope-o"></i>
                            <?php if($this->context->getUnread()):?><span class="count"><?=$this->context->getUnread()?></span><?php endif;?>
                        </a>
                        <?php
                        /**
                         * @var $messages \common\models\FeedBack[]
                         */
                            $messages = $this->context->getLastUnreadMessages()
                        ?>
                        <?php if(!empty($messages)):?>
                        <ul class="dropdown-menu notifications-list messages-list">
                            <li class="pointer">
                                <div class="pointer-inner">
                                    <div class="arrow"></div>
                                </div>
                            </li>
                            <?php foreach($messages as $message):?>
                            <li class="item first-item">
                                <a href="#">
                                    <?php if($user = \frontend\models\User::findByEmail($message->email)):?>
                                        <img src="<?=$user->profile->getAvatar('small')?>" alt=""/>
                                    <?php else:?>
                                        <img src="/admin/img/placeholder5.jpg" alt=""/>
                                    <?php endif;?>

										<span class="content">
											<span class="content-headline">
												<?=$message->name;?>
											</span>
											<span class="content-text">
												<?=\yii\helpers\StringHelper::truncate($message->body,104)?>
											</span>
										</span>
                                    <span class="time"><i class="fa fa-clock-o"></i><?=Yii::$app->formatter->asRelativeTime($message->create_at);?></span>
                                </a>
                            </li>
                            <?php endforeach;?>
                            <li class="item-footer">
                                <a href="<?= \yii\helpers\Url::to(['messages','#'=>'inbox'])?>">
                                    View all messages
                                </a>
                            </li>
                        </ul>
                        <?php endif;?>
                    </li>
                    <li class="dropdown profile-dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <span class="hidden-xs"><?=Yii::$app->user->getIdentity()->username?></span> <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="#change-password" class="md-trigger" data-modal="modal-1"><i class="fa fa-refresh"></i>Change password</a></li>
                            <li><a href="<?= \yii\helpers\Url::to(['setting'])?>"><i class="fa fa-cog"></i>Settings</a></li>
                            <li><a href="<?= \yii\helpers\Url::to(['messages','#'=>'inbox'])?>"><i class="fa fa-envelope-o"></i>Messages</a></li>
                            <li><a href="<?= \yii\helpers\Url::to(['logout'])?>"><i class="fa fa-power-off"></i>Logout</a></li>
                        </ul>
                    </li>
                    <li class="hidden-xxs">
                        <a href="<?= \yii\helpers\Url::to(['logout'])?>" class="btn">
                            <i class="fa fa-power-off"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</header>