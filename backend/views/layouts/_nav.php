<div id="nav-col">
<section id="col-left" class="col-left-nano">
<div id="col-left-inner" class="col-left-nano-content">
<div class="collapse navbar-collapse navbar-ex1-collapse" id="sidebar-nav">
<ul class="nav nav-pills nav-stacked">
<li class="<?=Yii::$app->controller->action->getUniqueId()=='site/index'?'active':''?>">
    <a href="<?=\yii\helpers\Url::base()?>" >
        <i class="fa fa-dashboard"></i>
        <span>Dashboard</span>
    </a>
</li>
<li class="<?=in_array(Yii::$app->controller->action->getUniqueId(),['site/managers','site/users'])?'open active':'';?>">
    <a href="#" class="dropdown-toggle">
        <i class="fa fa-users"></i>
        <span>Users</span>
        <i class="fa fa-chevron-circle-right drop-icon"></i>
    </a>
    <ul class="submenu">
        <li>
            <a href="<?=\yii\helpers\Url::to(['managers'])?>" class="<?=Yii::$app->controller->action->getUniqueId()=='site/managers'?'active':''?>">
                Administrators
            </a>
        </li>
        <li>
            <a href="<?=\yii\helpers\Url::to(['users'])?>" class="<?=Yii::$app->controller->action->getUniqueId()=='site/users'?'active':''?>">
                Members
            </a>
        </li>
    </ul>
</li>
<li class="<?=Yii::$app->controller->action->getUniqueId()=='site/partners'?'active':''?>">
    <a href="<?=\yii\helpers\Url::to(['partners'])?>" >
        <i class="fa fa-heart"></i>
        <span>Partners</span>
    </a>
</li>
<li class="<?=Yii::$app->controller->action->getUniqueId()=='site/payments'?'active':''?>">
    <a href="<?=\yii\helpers\Url::to(['payments'])?>" >
        <i class="fa fa-credit-card"></i>
        <span>Payments</span>
    </a>
</li>
<li class="<?=in_array(Yii::$app->controller->action->getUniqueId(),['site/messages','site/compose'])?'open active':'';?>">
    <a href="#" class="dropdown-toggle">
        <i class="fa fa-envelope"></i>
        <span>Email</span>
        <i class="fa fa-chevron-circle-right drop-icon"></i>
    </a>
    <ul class="submenu">
        <li>
            <a href="<?= \yii\helpers\Url::to(['messages','#'=>'inbox'])?>" class="<?=Yii::$app->controller->action->getUniqueId()=='site/messages'?'active':''?>">
                Inbox
            </a>
        </li>
        <li>
            <a href="<?= \yii\helpers\Url::to(['compose'])?>" class="<?=Yii::$app->controller->action->getUniqueId()=='site/compose'?'active':''?>">
                Compose
            </a>
        </li>
    </ul>
</li>
<li class="<?=Yii::$app->controller->action->getUniqueId()=='site/setting'?'active':''?>">
    <a href="<?=\yii\helpers\Url::to(['setting'])?>" >
        <i class="fa fa-cog"></i>
        <span>Settings</span>
    </a>
</li>
</ul>
</div>
</div>
</section>
</div>