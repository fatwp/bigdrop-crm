<?php
/* @var $this yii\web\View */
/* @var $models common\models\FeedBack[] */
?>
<?php foreach($models as $model):?>
    <li class="<?=$model->read==0 && !$model->sent?'unread':''?> clickable-row" data-id="<?=$model->id?>" data-href="<?=\yii\helpers\Url::to(['message-view','message'=>$model->id])?>">
        <div class="chbox">
            <div class="checkbox-nice">
                <input type="checkbox" id="m-checkbox-<?=$model->id?>" />
                <label for="m-checkbox-<?=$model->id?>"></label>
            </div>
        </div>
        <div class="star">
            <a <?=$model->starred?'class="starred"':''?>></a>
        </div>
        <div class="name">
            <?=$model->name;?>
        </div>
        <div class="message">
            <span class="subject"><?=$model->subject?$model->subject:'(no subject)';?> -</span>
            <span class="body"><?=\yii\helpers\StringHelper::truncate(strip_tags(\yii\helpers\Html::decode($model->body)),57)?></span>
        </div>
        <div class="meta-info">
            <span class="date"><?=Yii::$app->formatter->asRelativeTime($model->create_at);?></span>
        </div>
    </li>
<?php endforeach;?>