<?php
use frontend\models\User;
use yii\helpers\Html;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $model common\models\FeedBack */
\backend\assets\ComposeAsset::register($this);
\backend\assets\IE9Asset::register($this);
$this->params['layout'] = 'messages';
$this->title = 'compose new mail';
$emails = explode(',',$model->email);
$emails = filter_var_array($emails,FILTER_VALIDATE_EMAIL);
array_walk($emails,function(&$item,$key){
    $email = $item;
    $item = [];
    $item['text'] = $email;
    $item['slug'] = $email;
    $item['id']   = $email;
});
$emails = \yii\helpers\Json::encode($emails);

$script =<<<INLINE
   	$(document).ready(function() {

   	    $("#compose-form").submit(function(){
   	        var newOption = document.createElement("input");
		    newOption.name = "FeedBack[body]";
		    newOption.type = 'hidden';
		    newOption.value = $.trim($("#editor").html());
		    this.appendChild(newOption);

   	    });

		$('#email-list li > .star > a').on('click', function() {
			$(this).toggleClass('starred');
		});

		$(".has-tooltip").each(function (index, el) {
			$(el).tooltip({
				placement: $(this).data("placement") || 'bottom'
			});
		});

		setHeightEmailContent();

		initEmailScroller();

		$('.email-recepients').select2({
		    initSelection: function(element, callback) {
                callback($emails);
            },
			placeholder: 'Enter recepients',
			minimumInputLength: 2,
            tags: [],
            ajax: {
                url: '/admin/emails',
                dataType: 'json',
                type: "GET",
                quietMillis: 50,
                data: function (term) {
                    return {
                        term: term
                    };
                },
                results: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item,
                                slug: item,
                                id: item
                            }
                        })
                    };
                }
            }

		});
	});

	$(window).smartresize(function(){
		setHeightEmailContent();

		initEmailScroller();
	});

	function setHeightEmailContent() {
		if ($( document ).width() >= 992) {
			var windowHeight = $(window).height();
			var staticContentH = $('#header-navbar').outerHeight() + $('#email-header').outerHeight();
			staticContentH += ($('#email-box').outerHeight() - $('#email-box').height());

			$('#email-new').css('height', windowHeight - staticContentH);
		}
		else {
			$('#email-new').css('height', '');
		}
	}

	function initEmailScroller() {
		if ($( document ).width() >= 992) {
			$('#email-navigation').nanoScroller({
		    	alwaysVisible: false,
		    	iOSNativeScrolling: false,
		    	preventPageScrolling: true,
		    	contentClass: 'email-nav-nano-content'
		    });

			$('#email-new').nanoScroller({
		    	alwaysVisible: false,
		    	iOSNativeScrolling: false,
		    	preventPageScrolling: true,
		    	contentClass: 'email-new-nano-content'
		    });
		}
	}

	$(function(){
		function initToolbarBootstrapBindings() {
			var fonts = ['Serif', 'Sans', 'Arial', 'Arial Black', 'Courier',
						'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande', 'Lucida Sans', 'Tahoma', 'Times',
						'Times New Roman', 'Verdana'],
				fontTarget = $('[title=Font]').siblings('.dropdown-menu');

			$.each(fonts, function (idx, fontName) {
				fontTarget.append($('<li><a data-edit="fontName ' + fontName +'" style="font-family:\''+ fontName +'\'">'+fontName + '</a></li>'));
			});
			$('a[title]').tooltip({container:'body'});
			$('.dropdown-menu input').click(function() {return false;})
				.change(function () { $(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');})
				.keydown('esc', function () {this.value='';$(this).change();});

			$('[data-role=magic-overlay]').each(function () {
				var overlay = $(this), target = $(overlay.data('target'));
				overlay.css('opacity', 0).css('position', 'absolute').offset(target.offset()).width(target.outerWidth()).height(target.outerHeight());
			});
			if ("onwebkitspeechchange"	in document.createElement("input")) {
				var editorOffset = $('#editor').offset();
				$('#voiceBtn').css('position','absolute').offset({top: editorOffset.top, left: editorOffset.left+$('#editor').innerWidth()-35});
			} else {
				$('#voiceBtn').hide();
			}
		};
		function showErrorAlert (reason, detail) {
			var msg='';
			if (reason==='unsupported-file-type') { msg = "Unsupported format " +detail; }
			else {
				console.log("error uploading file", reason, detail);
			}
			$('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>'+
			 '<strong>File upload error</strong> '+msg+' </div>').prependTo('#alerts');
		};

		initToolbarBootstrapBindings();

		$('#editor').wysiwyg({ fileUploadError: showErrorAlert} );
	});
INLINE;

$this->registerJs($script,\yii\web\View::POS_END);
?>

<div class="row">
    <div class="col-lg-12">

        <div id="email-box" class="clearfix">
            <div class="row">
                <div class="col-lg-12">

                    <div id="email-header-mobile" class="visible-xs visible-sm clearfix">
                        <div id="email-header-title-mobile" class="pull-left">
                            <i class="fa fa-inbox"></i> Inbox
                        </div>

                        <a href="<?= \yii\helpers\Url::to(['compose'])?>" class="btn btn-success email-compose-btn pull-right">
                            <i class="fa fa-pencil-square-o"></i> Compose email
                        </a>
                    </div>

                    <header id="email-header" class="clearfix">
                        <div id="email-header-title" class="visible-md visible-lg">
                            <i class="fa fa-inbox"></i> Inbox
                        </div>

                        <div id="email-header-tools">
                            <div class="btn-group">
                                <a href="<?= \yii\helpers\Url::to(['messages','#'=>'inbox'])?>" class="btn btn-primary">
                                    <i class="fa fa-chevron-left"></i> Back to inbox
                                </a>
                            </div>
                        </div>

                    </header>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div id="email-navigation" class="email-nav-nano hidden-xs hidden-sm">
                        <div class="email-nav-nano-content">
                            <a href="<?= \yii\helpers\Url::to(['compose'])?>" class="btn btn-success email-compose-btn">
                                <i class="fa fa-pencil-square-o"></i> Compose email
                            </a>

                            <ul id="email-nav-items" class="clearfix">
                                <li >
                                    <a data-filter="inbox" href="<?= \yii\helpers\Url::to(['messages','#'=>'inbox'])?>">
                                        <i class="fa fa-inbox"></i>
                                        Inbox
                                        <span class="label label-primary pull-right"><?=$this->context->getUnread()?></span>
                                    </a>
                                </li>
                                <li>
                                    <a data-filter="starred" href="<?= \yii\helpers\Url::to(['messages','#'=>'starred'])?>">
                                        <i class="fa fa-star"></i>
                                        Starred

                                    </a>
                                </li>
                                <li>
                                    <a data-filter="sent" href="<?= \yii\helpers\Url::to(['messages','#'=>'sent'])?>">
                                        <i class="fa fa-envelope"></i>
                                        Sent

                                    </a>
                                </li>
                                <li>
                                    <a data-filter="trash" href="<?= \yii\helpers\Url::to(['messages','#'=>'trash'])?>">
                                        <i class="fa fa-trash-o"></i>
                                        Trash
                                    </a>
                                </li>
                            </ul>

                        </div>

                    </div>
                    <div id="email-new" class="email-new-nano">
                        <div class="email-new-nano-content">
                            <div id="email-new-inner">
                                <div id="email-new-title" class="clearfix">
                                    <span class="subject">New Email</span>
                                </div>

                                <?=Html::beginForm('','post',['id'=>'compose-form'])?>
                                    <div id="email-new-header">
                                        <div class="row form-group">
                                            <label for="exampleInpTo" class="col-md-2">To:</label>
                                            <div class="col-md-10">
                                                <input type="text" name="FeedBack[email]" placeholder="Enter recepients" id="exampleInpTo" class="form-control-s2 email-recepients" value="<?=$model->email?>"/>

                                            </div>
                                        </div>

                                        <div class="row form-group">
                                            <label for="exampleInpSubject" class="col-md-2">Subject:</label>
                                            <div class="col-md-10">
                                                <input type="text" value="<?=$model->subject?>" name="FeedBack[subject]" placeholder="Enter subject" id="exampleInpSubject" class="form-control"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="email-new-body">
                                        <div class="row">
                                            <label class="visible-xs">Content:</label>

                                            <div class="col-md-12">
                                                <div id="alerts"></div>
                                                <div class="btn-toolbar editor-toolbar hidden-xs" data-role="editor-toolbar" data-target="#editor">
                                                    <div class="btn-group">
                                                        <a class="btn btn-default dropdown-toggle" data-toggle="dropdown" title="Font"><i class="fa fa-font"></i><b class="caret"></b></a>
                                                        <ul class="dropdown-menu">
                                                        </ul>
                                                    </div>
                                                    <div class="btn-group">
                                                        <a class="btn btn-default dropdown-toggle" data-toggle="dropdown" title="Font Size"><i class="fa fa-text-height"></i>&nbsp;<b class="caret"></b></a>
                                                        <ul class="dropdown-menu">
                                                            <li><a data-edit="fontSize 5"><font size="5">Huge</font></a></li>
                                                            <li><a data-edit="fontSize 3"><font size="3">Normal</font></a></li>
                                                            <li><a data-edit="fontSize 1"><font size="1">Small</font></a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="btn-group">
                                                        <a class="btn btn-default" data-edit="bold" title="Bold (Ctrl/Cmd+B)"><i class="fa fa-bold"></i></a>
                                                        <a class="btn btn-default" data-edit="italic" title="Italic (Ctrl/Cmd+I)"><i class="fa fa-italic"></i></a>
                                                        <a class="btn btn-default" data-edit="strikethrough" title="Strikethrough"><i class="fa fa-strikethrough"></i></a>
                                                        <a class="btn btn-default" data-edit="underline" title="Underline (Ctrl/Cmd+U)"><i class="fa fa-underline"></i></a>
                                                    </div>
                                                    <div class="btn-group">
                                                        <a class="btn btn-default" data-edit="insertunorderedlist" title="Bullet list"><i class="fa fa-list-ul"></i></a>
                                                        <a class="btn btn-default" data-edit="insertorderedlist" title="Number list"><i class="fa fa-list-ol"></i></a>
                                                        <a class="btn btn-default" data-edit="outdent" title="Reduce indent (Shift+Tab)"><i class="fa fa-dedent"></i></a>
                                                        <a class="btn btn-default" data-edit="indent" title="Indent (Tab)"><i class="fa fa-indent"></i></a>
                                                    </div>
                                                    <div class="btn-group">
                                                        <a class="btn btn-default" data-edit="justifyleft" title="Align Left (Ctrl/Cmd+L)"><i class="fa fa-align-left"></i></a>
                                                        <a class="btn btn-default" data-edit="justifycenter" title="Center (Ctrl/Cmd+E)"><i class="fa fa-align-center"></i></a>
                                                        <a class="btn btn-default" data-edit="justifyright" title="Align Right (Ctrl/Cmd+R)"><i class="fa fa-align-right"></i></a>
                                                        <a class="btn btn-default" data-edit="justifyfull" title="Justify (Ctrl/Cmd+J)"><i class="fa fa-align-justify"></i></a>
                                                    </div>
                                                    <div class="btn-group">
                                                        <a class="btn btn-default" data-edit="undo" title="Undo (Ctrl/Cmd+Z)"><i class="fa fa-undo"></i></a>
                                                        <a class="btn btn-default" data-edit="redo" title="Redo (Ctrl/Cmd+Y)"><i class="fa fa-repeat"></i></a>
                                                    </div>
                                                    <input type="text" data-edit="inserttext" id="voiceBtn" x-webkit-speech="">
                                                </div>

                                                <div id="editor" class="wysiwyg-editor">
                                                    <?=$model->body?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="email-new-footer">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-10 col-md-offset-2">
                                                <div class="pull-right">
                                                    <div class="btn-group">
                                                        <button type="submit" class="btn btn-success"><i class="fa fa-send"></i> Send email</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                <?php Html::endForm();?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>

