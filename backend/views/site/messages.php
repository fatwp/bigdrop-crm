<?php
use frontend\models\User;
use yii\helpers\Html;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $models common\models\FeedBack[] */
\backend\assets\AppAsset::register($this);
\backend\assets\IE9Asset::register($this);
$this->params['layout'] = 'messages';
$this->title = 'mails';
$script =<<<INLINE
    $(document).ready(function() {

        if(!location.hash){
            location.hash = 'inbox';
        }

        $.get('',{filter:location.hash.substr(1) },function(re){ $($('#email-navigation').find('li a[data-filter='+location.hash.substr(1)+']').parents('li').get(0)).addClass('active'); $('#email-list').html(re);});

		$('#email-list').on('click','li > .star > a', function() {
		    var that = $(this);
		    $.get('',{toggle:'starred',message:$(that.parents('li').get(0)).data('id')},function(re){ if(re =='success') that.toggleClass('starred'); });
		});

		$('#email-nav-items li a').click(function(){
		    var that = $(this);
		    $.get('',{filter:that.data('filter')},function(re){
		     $(that.parents('li').get(0)).addClass('active').siblings('li').removeClass('active');
                 if(!$("#email-filter ul.dropdown-menu li a[data-filter=none]").find('i').get(0))
                    $($("#email-filter ul.dropdown-menu li a[data-filter=none]").prepend('<i class="fa fa-check"></i> ').parents('li').get(0)).siblings('li').find('i').remove();
                    if('starred'==that.data('filter')){
                        $("#email-filter ul.dropdown-menu li a[data-filter*=starred]").hide();
                        $("#email-filter ul.dropdown-menu li a[data-filter*=read]").show();
                    }else if('sent'==that.data('filter')){
                        $("#email-filter ul.dropdown-menu li a[data-filter*=read]").hide();
                        $("#email-filter ul.dropdown-menu li a[data-filter*=starred]").show();
                    }else{
                        $("#email-filter ul.dropdown-menu li a").show();
                    }
                 $('#email-list').html(re);
		     });
		});

		$(".has-tooltip").each(function (index, el) {
			$(el).tooltip({
				placement: $(this).data("placement") || 'bottom'
			});
		});

		$("#email-filter ul.dropdown-menu li a").click(function(){
		    var that = $(this);
		    if(!that.find('i').get(0)){
		        $.get('',{filter:location.hash.substr(1),filter2:that.data('filter')},function(re){ $(that.prepend('<i class="fa fa-check"></i> ').parents('li').get(0)).siblings('li').find('i').remove(); $('#email-list').html(re);});
		    }

		});

		$("#refresh-emails").click(function(){

		    var filter1 = $($("#email-nav-items li.active").find('a').get(0)).data('filter');
		    var filter2 = $($("#email-filter ul.dropdown-menu li a i").parents('a').get(0)).data('filter');

		    $.get('',{filter:filter1,filter2:filter2},function(re){  $('#email-list').html(re);});
		});
		$("#delete-emails").click(function(){

            var filter1 = $($("#email-nav-items li.active").find('a').get(0)).data('filter');
		    var filter2 = $($("#email-filter ul.dropdown-menu li a i").parents('a').get(0)).data('filter');
		    var delete_all  = [];
		    $("#email-list li input[type=checkbox]").each(function(){
		        if($(this).is(":checked"))
		            delete_all.push($($(this).parents('li.clickable-row').get(0)).data('id'));
		    });
		    if(delete_all.length==0){
		        alert('please select mails');
		    }else{
		        if(confirm(filter1!='trash'?'You really want to move this mails to trash?':'You really want to remove this mails definitely?')){
		            $.get('',{filter:filter1,filter2:filter2,delete:delete_all},function(re){  $('#email-list').html(re);});
		        }
		    }



		});

		setHeightEmailContent();

		initEmailScroller();

		$("#email-list").on('click',".clickable-row > div:not(.chbox,.star)",function(e) {
			if ((e.target instanceof HTMLAnchorElement) == true) {
				return;
			}

			var href = $(this).parent().data('href');

			if (href != '' && typeof href != 'undefined') {
				window.document.location = href;
			}
		});
	});

	$(window).smartresize(function(){
		setHeightEmailContent();

		initEmailScroller();
	});

	function setHeightEmailContent() {
		if ($( document ).width() >= 992) {
			var windowHeight = $(window).height();
			var staticContentH = $('#header-navbar').outerHeight() + $('#email-header').outerHeight();
			staticContentH += ($('#email-box').outerHeight() - $('#email-box').height());

			$('#email-content').css('height', windowHeight - staticContentH);
		}
		else {
			$('#email-content').css('height', '');
		}
	}

	function initEmailScroller() {
		if ($( document ).width() >= 992) {
			$('#email-navigation').nanoScroller({
		    	alwaysVisible: false,
		    	iOSNativeScrolling: false,
		    	preventPageScrolling: true,
		    	contentClass: 'email-nav-nano-content'
		    });

			$('#email-content').nanoScroller({
		    	alwaysVisible: false,
		    	iOSNativeScrolling: false,
		    	preventPageScrolling: true,
		    	contentClass: 'email-content-nano-content'
		    });
		}
	}
INLINE;

$this->registerJs($script,\yii\web\View::POS_END);
?>

    <div class="row">
        <div class="col-lg-12">

            <div id="email-box" class="clearfix">
                <div class="row">
                    <div class="col-lg-12">

                        <div id="email-header-mobile" class="visible-xs visible-sm clearfix">
                            <div id="email-header-title-mobile" class="pull-left">
                                <i class="fa fa-inbox"></i> Inbox
                            </div>

                            <a href="<?= \yii\helpers\Url::to(['compose'])?>" class="btn btn-success email-compose-btn pull-right">
                                <i class="fa fa-pencil-square-o"></i> Compose email
                            </a>
                        </div>

                        <header id="email-header" class="clearfix">
                            <div id="email-header-title" class="visible-md visible-lg">
                                <i class="fa fa-inbox"></i> Inbox
                            </div>

                            <div id="email-header-tools">
                                <div id="email-filter" class="btn-group">
                                    <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle has-tooltip" type="button" title="Filter by">
                                        <!--<i class="fa fa-square-0"></i> <span class="caret"></span>-->
                                        Filter by
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a data-filter="none" href="javascript:void(0)"><i class="fa fa-check"></i> All</a></li>
                                        <li><a data-filter="read" href="javascript:void(0)">Read</a></li>
                                        <li><a data-filter="unread"  href="javascript:void(0)">Unread</a></li>
                                        <li><a data-filter="starred" href="javascript:void(0)">Starred</a></li>
                                        <li><a data-filter="unstarred" href="javascript:void(0)">Unstarred</a></li>
                                    </ul>
                                </div>

                                <div class="btn-group">
                                    <button id="refresh-emails" class="btn btn-primary" type="button" title="Refresh" data-toggle="tooltip" data-placement="bottom">
                                        <i class="fa fa-refresh"></i>
                                    </button>
                                    <button id="delete-emails" class="btn btn-primary" type="button" title="Erase" data-toggle="tooltip" data-placement="bottom">
                                        <i class="fa fa-trash-o"></i>
                                    </button>
                                </div>
                            </div>

                        </header>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div id="email-navigation" class="email-nav-nano hidden-xs hidden-sm">
                            <div class="email-nav-nano-content">
                                <a href="<?= \yii\helpers\Url::to(['compose'])?>" class="btn btn-success email-compose-btn">
                                    <i class="fa fa-pencil-square-o"></i> Compose email
                                </a>

                                <ul id="email-nav-items" class="clearfix">
                                    <li>
                                        <a data-filter="inbox" href="#inbox">
                                            <i class="fa fa-inbox"></i>
                                            Inbox
                                            <span class="label label-primary pull-right"><?=$this->context->getUnread()?></span>
                                        </a>
                                    </li>
                                    <li>
                                        <a data-filter="starred" href="#starred">
                                            <i class="fa fa-star"></i>
                                            Starred

                                        </a>
                                    </li>
                                    <li>
                                        <a data-filter="sent" href="#sent">
                                            <i class="fa fa-envelope"></i>
                                            Sent

                                        </a>
                                    </li>
                                    <li>
                                        <a data-filter="trash" href="#trash">
                                            <i class="fa fa-trash-o"></i>
                                            Trash
                                        </a>
                                    </li>
                                </ul>

                            </div>

                        </div>
                        <div id="email-content" class="email-content-nano">
                            <div class="email-content-nano-content">
                                <ul id="email-list">

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>

