<?php
\backend\assets\EditableAsset::register($this);
\backend\assets\IE9Asset::register($this);
/**@var $settings \common\models\Setting[]*/
$timezones = timezone_identifiers_list();
$timezones = json_encode(array_combine($timezones,$timezones));
$script =<<<GALLERY
    $('#supportEmail').editable({
        ajaxOptions: {
            type: 'get',
            dataType: 'json'
        },
        send: 'always',
        success: function(response, newValue) {
            if(!response.success) return response.msg;

        }

    });
    $('#adminEmail').editable({
        ajaxOptions: {
            type: 'get',
            dataType: 'json'
        },
        url:'',
        success: function(response, newValue) {
            if(!response.success) return response.msg;
        }
    });
    var timezones = [];
    $.each($timezones, function(k, v) {
        timezones.push({id: k, text: v});
    })
    $('#backendTimezone').editable({
        ajaxOptions: {
            type: 'get',
            dataType: 'json'
        },
        url:'',
        source: timezones,
        select2: {
            width: 200,
            allowClear: true
        },
        success: function(response, newValue) {
            if(!response.success) return response.msg;
        }
    });

    $('#newsFeed').editable({
            ajaxOptions: {
                type: 'get',
                dataType: 'json'
            },
            url:'',
			source: [
				{value: 'true', text: 'true'},
				{value: 'false', text: 'false'}
			],
			select2: {
				width: 200,
				allowClear: true
			},
            success: function(response, newValue) {
                if(!response.success) return response.msg;
            }
    });

    $('#user .editable').on('hidden', function(e, reason){
        if(reason === 'save' || reason === 'nochange') {
            var _next = $(this).closest('tr').next().find('.editable');
                setTimeout(function() {
                    _next.editable('show');
                }, 300);
        }
    });
GALLERY;

$this->registerJs($script,\yii\web\View::POS_END);

?>

    <div class="row">
        <div class="col-lg-12">

            <div class="row">
                <div class="col-lg-12">
                    <ol class="breadcrumb">
                        <li><a href="<?=\yii\helpers\Url::base()?>">Home</a></li>
                        <li class="active"><span>Settings</span></li>
                    </ol>

                    <h1>Settings</h1>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="main-box clearfix">
                        <header class="main-box-header clearfix">
                            <h2>Settings</h2>
                        </header>

                        <div class="main-box-body clearfix">
                            <div class="table-responsive">
                                <table id="user" class="table table-hover" style="clear: both">
                                    <thead>
                                    <tr>
                                        <th><span>Type</span></th>
                                        <th><span>Click to edit</span></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($settings as $setting):?>
                                        <tr>
                                            <td width="35%"><?=$setting->description?></td>
                                            <td width="65%"><a href="#" id="<?=$setting->property?>" data-url="<?=\yii\helpers\Url::to([''])?>" data-type="<?=in_array($setting->property,['newsFeed','backendTimezone'])?'select2':'text'?>" data-pk="<?=$setting->id?>" data-title="<?=$setting->description?>" data-value="<?=$setting->value?>" class="editable editable-click"><?=$setting->value?></a></td>
                                        </tr>
                                    <?php endforeach;?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="pull-right">
                                <a href="<?=\yii\helpers\Url::to(['','default'=>'true'])?>">default settings</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

