<?php
use frontend\models\User;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $model frontend\models\User */
/* @var $dataProvider yii\data\ActiveDataProvider */
\backend\assets\AppAsset::register($this);
\backend\assets\IE9Asset::register($this);
?>
<div class="row">
    <div class="col-lg-12">

        <div class="row">
            <div class="col-lg-12">
                <ol class="breadcrumb">
                    <li><a href="<?=\yii\helpers\Url::base()?>">Home</a></li>
                    <li class="active"><span>Members</span></li>
                </ol>

                <div class="clearfix">
                    <h1 class="pull-left">Members</h1>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="main-box no-header clearfix">
                    <div class="main-box-body clearfix">

                        <?php
                        Pjax::begin();
                        echo GridView::widget([
                            'dataProvider'  => $dataProvider,
                            'id'            => 'users',
                            'tableOptions'  => ['class'=>'table user-list table-hover'],
                            'options'       => ['class' => 'table-responsive'],
                            'pager'         => ['options'=>['class'=>'pagination pull-right']],
                            'filterModel'   => $model,
                            'columns'       => [
                                [
                                    'attribute'   => 'name',
                                    'format'      => 'Html',
                                    'value' => function ($data) {
                                        $returnVal  = '';
                                        $returnVal .= Html::img($data->profile->getAvatar('small'),['style'=>'border-radius:50%']);
                                        $returnVal .= Html::a($data->name,\yii\helpers\Url::to(['site/user-view','user'=>$data->id]),['class'=>'user-link']);
                                        $returnVal .= Html::tag('span','User',['class'=>'user-subhead']);
                                        return $returnVal;
                                    },
                                ],
                                [
                                    'attribute'   => 'email',
                                    'format'      => 'email',
                                ],
                                [
                                    'attribute'   => 'reg_date',
                                    'label'      => 'Registered',
                                    'format'      => ['date', 'php:m-d-Y'],
                                ],
                                [
                                    'attribute'   => 'tariff',
                                    'label'      => 'Tariff',
                                    'format'      => 'html',
                                    'filter'    => \common\models\Payment::getTariffList(true),
                                    'value'     =>function ($data) {
                                        $class  = 'label label-';
                                        $tariff  = \common\models\Payment::getTariffList(true)[$data->profile->tariff_id];
                                        $label = '';
                                        if(strcmp($tariff,'yearly')==0) {
                                            $class .= 'warning';
                                            $label  = 'Yearly';
                                        }else if(strcmp($tariff,'monthly')==0){
                                            $class .= 'info';
                                            $label  = 'Monthly';
                                        }else{
                                            $class .= 'default';
                                            $label  = 'Free';
                                        }
                                        return Html::tag('span',$label,['class'=>$class]);
                                    },
                                    'headerOptions' => ['class'=>'text-center'],
                                    'contentOptions'=>['class'=>'text-center'],
                                ],
                                [
                                    'attribute'     => 'status',
                                    'format'        => 'html',
                                    'filter'        => [
                                        User::STATUS_PENDING    => 'Pending',
                                        User::STATUS_ACTIVE     => 'Active',
                                        User::STATUS_SUSPENDED  => 'Baned',
                                        User::STATUS_DELETED    => 'Deleted',
                                    ],
                                    'value' => function ($data) {
                                        $class  = 'label';
                                        $label  = '';
                                        switch($data->status){
                                            case User::STATUS_PENDING: $class.= ' label-warning'; $label = 'Pending';break;
                                            case User::STATUS_ACTIVE:  $class.= ' label-success'; $label = 'Active';break;
                                            case User::STATUS_SUSPENDED: $class.= ' label-danger'; $label = 'Baned';break;
                                            case User::STATUS_DELETED: $class.= ' label-default'; $label = 'Deleted';break;
                                        }
                                        return Html::tag('span',$label,['class'=>$class]);
                                    },
                                    'headerOptions' => ['class'=>'text-center'],
                                    'contentOptions'=>['class'=>'text-center'],
                                ],


                            ],
                        ]);
                        Pjax::end();
                        ?>

                    </div>
                </div>
            </div>
        </div>

    </div>
</div>