<?php

use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\ManagerForm */
/* @var $form yii\bootstrap\ActiveForm */

\backend\assets\AppAsset::register($this);
\backend\assets\IE9Asset::register($this);
?>
<div class="row">
    <div class="col-lg-12">
        <div class="main-box">
            <header class="main-box-header clearfix">
                <h2>Manager</h2>
            </header>
            <div class="main-box-body clearfix">
                <?php
                $form = ActiveForm::begin([
                    'layout' => 'horizontal',
                    'options' => ['enctype' => 'multipart/form-data'],
                    'fieldConfig' => [
                        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                        'horizontalCssClasses' => [
                            'label' => 'col-lg-2',
                            'wrapper' => 'col-lg-6',
                        ],
                    ],
                ]);
                ?>
                <?= $form->field($model, 'email')
                    ->textInput()
                ?>
                <?= $form->field($model, 'role')
                    ->dropDownList([
                        \backend\models\User::ROLE_MODERATOR => 'Moderator',
                        \backend\models\User::ROLE_ADMIN     => 'Administrator',
                    ]);
                ?>
                <?= $form->field($model, 'password')
                    ->passwordInput()
                ?>
                <?= $form->field($model, 'confirm_password')
                    ->passwordInput()
                ?>
                <div class="form-group">
                    <div class="col-lg-offset-2 col-lg-10">
                        <button type="submit" class="btn btn-success">Create</button>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>