<?php
use frontend\models\User;
use yii\helpers\Html;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $model common\models\FeedBack */
\backend\assets\AppAsset::register($this);
\backend\assets\IE9Asset::register($this);
$this->params['layout'] = 'messages';
$this->title = 'view mail';
$script =<<<INLINE
   $(document).ready(function() {
		$('#email-list li > .star > a').on('click', function() {
			$(this).toggleClass('starred');
		});

		$(".has-tooltip").each(function (index, el) {
			$(el).tooltip({
				placement: $(this).data("placement") || 'bottom'
			});
		});

		setHeightEmailContent();

		initEmailScroller();
	});

	$(window).smartresize(function(){
		setHeightEmailContent();

		initEmailScroller();
	});

	function setHeightEmailContent() {
		if ($( document ).width() >= 992) {
			var windowHeight = $(window).height();
			var staticContentH = $('#header-navbar').outerHeight() + $('#email-header').outerHeight();
			staticContentH += ($('#email-box').outerHeight() - $('#email-box').height());

			$('#email-detail').css('height', windowHeight - staticContentH);
		}
		else {
			$('#email-detail').css('height', '');
		}
	}

	function initEmailScroller() {
		if ($( document ).width() >= 992) {
			$('#email-navigation').nanoScroller({
		    	alwaysVisible: false,
		    	iOSNativeScrolling: false,
		    	preventPageScrolling: true,
		    	contentClass: 'email-nav-nano-content'
		    });

			$('#email-detail').nanoScroller({
		    	alwaysVisible: false,
		    	iOSNativeScrolling: false,
		    	preventPageScrolling: true,
		    	contentClass: 'email-detail-nano-content'
		    });
		}
	}
INLINE;

$this->registerJs($script,\yii\web\View::POS_END);
?>

<div class="row">
    <div class="col-lg-12">

        <div id="email-box" class="clearfix">
            <div class="row">
                <div class="col-lg-12">

                    <div id="email-header-mobile" class="visible-xs visible-sm clearfix">
                        <div id="email-header-title-mobile" class="pull-left">
                            <i class="fa fa-inbox"></i> Inbox
                        </div>

                        <a href="<?= \yii\helpers\Url::to(['compose'])?>" class="btn btn-success email-compose-btn pull-right">
                            <i class="fa fa-pencil-square-o"></i> Compose email
                        </a>
                    </div>

                    <header id="email-header" class="clearfix">
                        <div id="email-header-title" class="visible-md visible-lg">
                            <i class="fa fa-inbox"></i> Inbox
                        </div>

                        <div id="email-header-tools">
                            <div class="btn-group">
                                <a href="<?= \yii\helpers\Url::to(['messages','#'=>'inbox'])?>" class="btn btn-primary">
                                    <i class="fa fa-chevron-left"></i> Back to inbox
                                </a>
                            </div>
                        </div>

                    </header>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div id="email-navigation" class="email-nav-nano hidden-xs hidden-sm">
                        <div class="email-nav-nano-content">
                            <a href="<?= \yii\helpers\Url::to(['compose'])?>" class="btn btn-success email-compose-btn">
                                <i class="fa fa-pencil-square-o"></i> Compose email
                            </a>

                            <ul id="email-nav-items" class="clearfix">
                                <li >
                                    <a data-filter="inbox" href="<?= \yii\helpers\Url::to(['messages','#'=>'inbox'])?>">
                                        <i class="fa fa-inbox"></i>
                                        Inbox
                                        <span class="label label-primary pull-right"><?=$this->context->getUnread()?></span>
                                    </a>
                                </li>
                                <li>
                                    <a data-filter="starred" href="<?= \yii\helpers\Url::to(['messages','#'=>'starred'])?>">
                                        <i class="fa fa-star"></i>
                                        Starred

                                    </a>
                                </li>
                                <li>
                                    <a data-filter="sent" href="<?= \yii\helpers\Url::to(['messages','#'=>'sent'])?>">
                                        <i class="fa fa-envelope"></i>
                                        Sent

                                    </a>
                                </li>
                                <li>
                                    <a data-filter="trash" href="<?= \yii\helpers\Url::to(['messages','#'=>'trash'])?>">
                                        <i class="fa fa-trash-o"></i>
                                        Trash
                                    </a>
                                </li>
                            </ul>

                        </div>

                    </div>
                    <div id="email-detail" class="email-detail-nano">
                        <div class="email-detail-nano-content">
                            <div id="email-detail-inner">
                                <div id="email-detail-subject" class="clearfix">
                                    <span class="subject"><?=$model->subject?$model->subject:'(no subject)';?></span>
                                </div>

                                <div id="email-detail-sender" class="clearfix">
                                    <div class="picture hidden-xs">
                                        <?php if($user = \frontend\models\User::findByEmail($model->email)):?>
                                            <img src="<?=$user->profile->getAvatar('medium')?>" alt=""/>
                                        <?php else:?>
                                            <img src="/admin/img/placeholder5.jpg" alt=""/>
                                        <?php endif;?>

                                    </div>

                                    <div class="users">
                                        <div class="from clearfix">
                                            <div class="name">
                                                <?=$model->sent?'PhotoSafe':$model->name?>
                                            </div>
                                            <div class="email hidden-xs">
                                                &lt;<?=$model->sent?Yii::$app->params['adminEmail']:$model->email?>&gt;
                                            </div>
                                        </div>

                                        <div class="to">
                                            To: <span><?=$model->inbox?'PhotoSafe':$model->name?></span>
                                        </div>
                                    </div>

                                    <div class="tools">
                                        <div class="date">
                                            <?=Yii::$app->formatter->asDate($model->create_at,'php:M d');?> (<?=Yii::$app->formatter->asRelativeTime($model->create_at);?>)
                                        </div>
                                        <div class="btns">
                                            <div class="btn-group">
                                                <a href="<?=\yii\helpers\Url::to(['site/compose','to'=>$model->email])?>" class="btn btn-success" type="button">
                                                    <i class="fa fa-mail-reply"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div id="email-body">
                                    <?=\yii\helpers\Html::decode($model->body)?>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>

