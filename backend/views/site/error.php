<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;
if(Yii::$app->user->isGuest || Yii::$app->user->identity->role === \common\models\User::ROLE_USER){
    $this->context->layout = 'error';
}
else{
    \backend\assets\AppAsset::register($this);
    \backend\assets\IE9Asset::register($this);
}
?>

<div class="row">
    <div class="col-lg-12">

        <div id="error-box">
            <div class="row">
                <div class="col-xs-12" style="min-height: 900px;">
                    <div id="error-box-inner">
                        <img src="img/error-404-v2.png" alt="Have you seen this page?"/>
                    </div>
                    <h1><?= Html::encode($this->title) ?></h1>
                    <?= nl2br(Html::encode($message)) ?>
                    <p>
                        Go back to <a href="/">homepage</a>.
                    </p>
                </div>
            </div>
        </div>

    </div>
</div>
