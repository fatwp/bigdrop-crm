<?php
use frontend\models\User;
/* @var $this yii\web\View */
/* @var $user frontend\models\User */

\backend\assets\UserAsset::register($this);
\backend\assets\IE9Asset::register($this);
$script =<<<GALLERY
    $(function() {
        $(document).ready(function() {
            $('#newsfeed .story-images').magnificPopup({
                type: 'image',
                delegate: 'a',
                gallery: {
                    enabled: true
                }
            });
        });
    });
GALLERY;

$this->registerJs($script,\yii\web\View::POS_END);

?>
<div class="row">
    <div class="col-lg-12">

        <div class="row">
            <div class="col-lg-12">
                <ol class="breadcrumb">
                    <li><a href="<?=\yii\helpers\Url::base()?>">Home</a></li>
                    <li><a href="<?=\yii\helpers\Url::to(['site/users'])?>">Users</a></li>
                    <li class="active"><span>User Profile</span></li>
                </ol>

                <h1>User Profile</h1>
            </div>
        </div>

        <div class="row" id="user-profile">
            <div class="col-lg-3 col-md-4 col-sm-4">
                <div class="main-box clearfix">
                    <header class="main-box-header clearfix">
                        <h2><?=$user->profile->getFullName()?></h2>
                    </header>

                    <div class="main-box-body clearfix">
                        <!--<div class="profile-status">
                            <i class="fa fa-circle"></i> Online
                        </div>-->

                        <img src="<?=$user->profile->getAvatar()?>" alt="" class="profile-img img-responsive center-block" />

                        <div class="profile-since">
                            Member since: <?=Yii::$app->formatter->asDate($user->reg_date, 'medium');?>
                        </div>

                        <div class="profile-details">
                            <ul class="fa-ul">
                                <li><i class="fa-li fa fa-circle "></i>Total: <span><?=$user->profile->getSpace('total')?></span></li>
                                <li><i class="fa-li fa  fa-upload  "></i>Used: <span><?=$user->profile->getSpace('used')?></span></li>
                                <li><i class="fa-li fa fa-circle-o"></i>Free: <span><?=$user->profile->getSpace('free')?></span></li>
                            </ul>
                        </div>

                        <div class="profile-message-btn center-block text-center">
                            <a href="<?=\yii\helpers\Url::to(['site/compose','to'=>$user->email])?>" class="btn btn-success">
                                <i class="fa fa-envelope"></i>
                                Send message
                            </a>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-lg-9 col-md-8 col-sm-8">
                <div class="main-box clearfix">
                    <div class="tabs-wrapper profile-tabs">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab-newsfeed" data-toggle="tab">Albums</a></li>
                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="tab-newsfeed">
                                <div id="newsfeed">

                                    <?php
                                    $albums = \common\models\Album::all($user->id,['id','name','create_at']);
                                    ?>
                                    <?php foreach($albums as $album):?>
                                        <div class="story">
                                            <div class="story-user">
                                                <a href="#">
                                                    <img src="<?=$user->profile->getAvatar('small')?>" alt="">
                                                </a>
                                            </div>
                                            <div class="story-content">
                                                <header class="story-header">
                                                    <div class="story-author">
                                                        <a href="<?=\yii\helpers\Url::to(['album-view','album'=>$album['id'],'user'=>$user->id])?>" class="story-author-link">
                                                            <?=$album['name']?>
                                                        </a>
                                                    </div>
                                                    <div class="story-time">
                                                        <i class="fa fa-clock-o"></i> <?=Yii::$app->formatter->asDate($album['create_at'], 'medium');?>
                                                    </div>
                                                </header>

                                                <div class="story-inner-content">
                                                    <?php
                                                    $images = \common\models\Album::preview($album['id'],$user->id);
                                                    $count  = \common\models\Album::count($album['id']);
                                                    $url    = \common\models\Album::getUrl($album['id'],$user->id);
                                                    $tmp    = \yii\helpers\Url::base().'/temp'
                                                    ?>
                                                    <div class="story-images clearfix">
                                                        <?php foreach($images as $image):?>
                                                            <a href="<?=$url.'/'.$image['name']?>" class="<?=$image['class']?>">
                                                                <img src="<?=$tmp.'/'.$image['name']?>" alt="" class="img-responsive"/>
                                                            </a>
                                                        <?php endforeach;?>
                                                    </div>
                                                </div>
                                                <footer class="story-footer">
                                                    <a href="<?=\yii\helpers\Url::to(['album-view','album'=>$album['id'],'user'=>$user->id])?>" class="story-comments-link">
                                                        <i class="fa fa-file-image-o"></i> <?=$count?> <?=$count==1?'photo':'photos'?>
                                                    </a>
                                                </footer>
                                            </div>
                                        </div>
                                    <?php endforeach;?>

                                </div>

                           </div>




                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>