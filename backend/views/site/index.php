<?php
use frontend\models\User;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;

/* @var $this yii\web\View */

\backend\assets\HomeAsset::register($this);
\backend\assets\IE9Asset::register($this);
$dynamic  = Yii::$app->db->createCommand("SELECT DATE(`date`) AS `period`, `uploaded_photos`, `used_space`  FROM `dynamic_statistic` ORDER BY `date` DESC  LIMIT 7")->queryAll();
$static   = Yii::$app->db->createCommand("SELECT `name`, `value`  FROM `static_statistic`")->queryAll();
$dynamic    = json_encode($dynamic);
$static   = \yii\helpers\ArrayHelper::map($static,'name','value');
$script =<<<INLINE
	$(document).ready(function() {


	    //CHARTS
		graphLine = Morris.Line({
			element: 'graph-line',
			data: $dynamic,
			lineColors: ['#ffffff'],
			xkey: 'period',
			ykeys: ['uploaded_photos'],
			labels: ['Uploaded photos'],
			pointSize: 3,
			hideHover: 'auto',
			gridTextColor: '#ffffff',
			gridLineColor: 'rgba(255, 255, 255, 0.3)',
			resize: true
		});


		$('.infographic-box .value .timer').countTo({});

	});
INLINE;
$this->registerJs($script,3);
$this->title = 'Dashboard|Photosafe';
?>

<div class="row">
<div class="col-lg-12">
<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active"><span>Dashboard</span></li>
        </ol>

        <h1>Dashboard</h1>
    </div>
</div>

<div class="row">
    <div class="col-lg-3 col-sm-6 col-xs-12">
        <div class="main-box infographic-box">
            <i class="fa fa-user yellow-bg"></i>
            <span class="headline">Pending users</span>
										<span class="value">
											<span class="timer" data-from="0" data-to="<?=$static['pending_users']?>" data-speed="5" data-refresh-interval="2">
												<?=$static['pending_users']?>
											</span>
										</span>
        </div>
    </div>
    <div class="col-lg-3 col-sm-6 col-xs-12">
        <div class="main-box infographic-box">
            <i class="fa fa-user green-bg"></i>
            <span class="headline">Active users</span>
										<span class="value">
											<span class="timer" data-from="0" data-to="<?=$static['active_users']?>" data-speed="5" data-refresh-interval="2">
												<?=$static['active_users']?>
											</span>
										</span>
        </div>
    </div>
    <div class="col-lg-3 col-sm-6 col-xs-12">
        <div class="main-box infographic-box">
            <i class="fa fa-user red-bg"></i>
            <span class="headline">Suspended users</span>
										<span class="value">
											<span class="timer" data-from="0" data-to="<?=$static['baned_users']?>" data-speed="5" data-refresh-interval="2">
												<?=$static['baned_users']?>
											</span>
										</span>
        </div>
    </div>
    <div class="col-lg-3 col-sm-6 col-xs-12">
        <div class="main-box infographic-box">
            <i class="fa fa-user gray-bg"></i>
            <span class="headline">Deleted users</span>
										<span class="value">
											<span class="timer" data-from="0" data-to="<?=$static['deleted_users']?>" data-speed="5">
												<?=$static['deleted_users']?>
											</span>
										</span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-9 col-lg-10">
        <div class="main-box">
            <div class="row">
                <div class="col-md-9">
                    <div class="graph-box emerald-bg">
                        <h2>Uploaded photos</h2>
                        <div class="graph" id="graph-line" style="max-height: 335px;"></div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="row graph-nice-legend">

                        <div class="graph-legend-row col-md-12 col-sm-4">
                            <div class="graph-legend-row-inner">
															<span class="graph-legend-name">
																Total users
															</span>
															<span class="graph-legend-value">
																<?=$static['deleted_users'] + $static['baned_users'] + $static['active_users'] + $static['pending_users']?>
															</span>
                            </div>
                        </div>
                        <div class="graph-legend-row col-md-12 col-sm-4">
                            <div class="graph-legend-row-inner">
															<span class="graph-legend-name">
																Total albums
															</span>
															<span class="graph-legend-value">
																<?=$static['albums']?>
															</span>
                            </div>
                        </div>
                        <div class="graph-legend-row col-md-12 col-sm-4">
                            <div class="graph-legend-row-inner">
															<span class="graph-legend-name">
																Used space
															</span>
															<span class="graph-legend-value">
																<?=\common\models\UploadedImage::getReadableFileSize($static['used_space'])?>
															</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-lg-2">
        <div class="social-box-wrapper">
            <div class="social-box col-md-12 col-sm-4 facebook">
                <i class="fa fa-facebook"></i>
                <div class="clearfix">
                    <span class="social-count">184k</span>
                    <span class="social-action">likes</span>
                </div>
                <span class="social-name">facebook</span>
            </div>
            <div class="social-box col-md-12 col-sm-4 twitter">
                <i class="fa fa-twitter"></i>
                <div class="clearfix">
                    <span class="social-count">49k</span>
                    <span class="social-action">tweets</span>
                </div>
                <span class="social-name">twitter</span>
            </div>
            <div class="social-box col-md-12 col-sm-4 google">
                <i class="fa fa-google-plus"></i>
                <div class="clearfix">
                    <span class="social-count">204</span>
                    <span class="social-action">circles</span>
                </div>
                <span class="social-name">google+</span>
            </div>
        </div>
    </div>
</div>
<?php
/* @var $provider yii\data\ActiveDataProvider */

$provider = new ActiveDataProvider([
    'query' => \frontend\models\User::find()->joinWith('profile')->select(['user.*',"CONCAT_WS(' ',first_name,last_name) AS name"]),
    'pagination' => [
        'pageSize' => 5,
    ],
    'sort'=>[
        'defaultOrder'=>[
            'reg_date' => SORT_DESC,
        ],
        'attributes' => [
            'reg_date',
        ],
    ]
]);
?>
<div class="row">
    <div class="col-lg-12">
        <div class="main-box no-header clearfix">
            <div class="main-box-body clearfix">

                <?php

                echo GridView::widget([
                    'dataProvider'  => $provider,
                    'id'            => 'users',
                    'tableOptions'  => ['class'=>'table user-list table-hover'],
                    'options'       => ['class' => 'table-responsive'],
                    'layout'        => '{items}',
                    'columns'       => [
                        [
                            'attribute'   => 'name',
                            'format'      => 'Html',
                            'value' => function ($data) {
                                $returnVal  = '';
                                $returnVal .= Html::img($data->profile->getAvatar('small'),['style'=>'border-radius:50%']);
                                $returnVal .= Html::a($data->name,\yii\helpers\Url::to(['site/user-view','user'=>$data->id]),['class'=>'user-link']);
                                $returnVal .= Html::tag('span','User',['class'=>'user-subhead']);
                                return $returnVal;
                            },
                        ],
                        [
                            'attribute'   => 'reg_date',
                            'enableSorting'=>false,
                            'label'      => 'Registered',
                            'format'      => ['date', 'php:m-d-Y'],
                        ],
                        [
                            'attribute'   => 'tariff_id',
                            'label'      => 'Tariff',
                            'filter'    =>false,
                            'format'      => 'html',
                            'value'     =>function ($data) {
                                $class  = 'label label-';
                                $tariff  = \common\models\Payment::getTariffList(true)[$data->profile->tariff_id];
                                $label = '';
                                if(strcmp($tariff,'yearly')==0) {
                                    $class .= 'warning';
                                    $label  = 'Yearly';
                                }else if(strcmp($tariff,'monthly')==0){
                                    $class .= 'info';
                                    $label  = 'Monthly';
                                }else{
                                    $class .= 'default';
                                    $label  = 'Free';
                                }
                                return Html::tag('span',$label,['class'=>$class]);
                            },
                        ],
                        [
                            'attribute'     => 'status',
                            'format'        => 'html',
                            'filter'        => [
                                User::STATUS_PENDING    => 'Pending',
                                User::STATUS_ACTIVE     => 'Active',
                                User::STATUS_SUSPENDED  => 'Baned',
                                User::STATUS_DELETED    => 'Deleted',
                            ],
                            'value' => function ($data) {
                                $class  = 'label';
                                $label  = '';
                                switch($data->status){
                                    case User::STATUS_PENDING: $class.= ' label-warning'; $label = 'Pending';break;
                                    case User::STATUS_ACTIVE:  $class.= ' label-success'; $label = 'Active';break;
                                    case User::STATUS_SUSPENDED: $class.= ' label-danger'; $label = 'Baned';break;
                                    case User::STATUS_DELETED: $class.= ' label-default'; $label = 'Deleted';break;
                                }
                                return Html::tag('span',$label,['class'=>$class]);
                            },
                            'headerOptions' => ['class'=>'text-center'],
                            'contentOptions'=>['class'=>'text-center'],
                        ],
                        [
                            'attribute'   => 'email',
                            'format'      => 'email',
                        ],

                    ],
                ]);
                ?>

            </div>
        </div>
    </div>
</div>

<!--<div class="row">
    <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="main-box small-graph-box red-bg">
            <span class="value">2.562</span>
            <span class="headline">Users</span>
            <div class="progress">
                <div style="width: 60%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="60" role="progressbar" class="progress-bar">
                    <span class="sr-only">60% Complete</span>
                </div>
            </div>
										<span class="subinfo">
											<i class="fa fa-arrow-circle-o-up"></i> 10% higher than last week
										</span>
										<span class="subinfo">
											<i class="fa fa-users"></i> 29 new users
										</span>
        </div>
    </div>
    <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="main-box small-graph-box emerald-bg">
            <span class="value">69.600</span>
            <span class="headline">Visits</span>
            <div class="progress">
                <div style="width: 84%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="84" role="progressbar" class="progress-bar">
                    <span class="sr-only">84% Complete</span>
                </div>
            </div>
										<span class="subinfo">
											<i class="fa fa-arrow-circle-o-down"></i> 22% less than last week
										</span>
										<span class="subinfo">
											<i class="fa fa-globe"></i> 84.912 last week
										</span>
        </div>
    </div>
    <div class="col-md-4 col-sm-6 col-xs-12 hidden-sm">
        <div class="main-box small-graph-box green-bg">
            <span class="value">923</span>
            <span class="headline">Orders</span>
            <div class="progress">
                <div style="width: 42%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="42" role="progressbar" class="progress-bar">
                    <span class="sr-only">42% Complete</span>
                </div>
            </div>
										<span class="subinfo">
											<i class="fa fa-arrow-circle-o-up"></i> 15% higher than last week
										</span>
										<span class="subinfo">
											<i class="fa fa-shopping-cart"></i> 8 new orders
										</span>
        </div>
    </div>
</div>-->


</div>
</div>
