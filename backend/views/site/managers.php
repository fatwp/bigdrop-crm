<?php
use frontend\models\User;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $model backend\models\User */
/* @var $dataProvider yii\data\ActiveDataProvider */
\backend\assets\AppAsset::register($this);
\backend\assets\IE9Asset::register($this);


?>
<div class="row">
    <div class="col-lg-12">

        <div class="row">
            <div class="col-lg-12">
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li class="active"><span>Administrators</span></li>
                </ol>

                <div class="clearfix">
                    <h1 class="pull-left">Administrators</h1>
                    <div class="pull-right top-page-ui">
                        <a href="<?=\yii\helpers\Url::to(['site/manager'])?>" class="btn btn-primary pull-right">
                            <i class="fa fa-plus-circle fa-lg"></i> Add admin
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="main-box no-header clearfix">
                    <div class="main-box-body clearfix">

                        <?php
                        Pjax::begin();
                        echo GridView::widget([
                            'dataProvider'  => $dataProvider,
                            'id'            => 'managers',
                            'tableOptions'  => ['class'=>'table user-list table-hover'],
                            'options'       => ['class' => 'table-responsive'],
                            'pager'         => ['options'=>['class'=>'pagination pull-right']],
                            //'filterModel'   => $model,
                            'columns'       => [
                                [
                                    'attribute'   => 'role',
                                    'format'      => 'Html',
                                    'value' => function ($data) {
                                        $role = '';
                                        switch($data->role){
                                            case User::ROLE_ADMIN: $role ='Admin'; break;
                                            case User::ROLE_MODERATOR: $role ='Moderator'; break;
                                        }
                                        $returnVal  = '';
                                        $returnVal .= Html::img('/admin/img/placeholder5.jpg');
                                        $returnVal .= Html::a($data->email,'#',['class'=>'user-link']);
                                        $returnVal .= Html::tag('span',$role,['class'=>'user-subhead']);
                                        return $returnVal;
                                    },
                                ],
                                [
                                    'attribute'   => 'email',
                                    'format'      => 'email',
                                ],
                                [
                                    'attribute'   => 'reg_date',
                                    'label'      => 'Registered',
                                    'format'      => ['date', 'php:m-d-Y'],
                                ],
                                [
                                    'attribute'     => 'status',
                                    'format'        => 'html',
                                    'filter'        => [
                                        User::STATUS_PENDING    => 'Pending',
                                        User::STATUS_ACTIVE     => 'Active',
                                        User::STATUS_SUSPENDED  => 'Baned',
                                        User::STATUS_DELETED    => 'Deleted',
                                    ],
                                    'value' => function ($data) {
                                        $class  = 'label';
                                        $label  = '';
                                        switch($data->status){
                                            case User::STATUS_PENDING: $class.= ' label-warning'; $label = 'Pending';break;
                                            case User::STATUS_ACTIVE:  $class.= ' label-success'; $label = 'Active';break;
                                            case User::STATUS_SUSPENDED: $class.= ' label-danger'; $label = 'Baned';break;
                                            case User::STATUS_DELETED: $class.= ' label-default'; $label = 'Deleted';break;
                                        }
                                        return Html::tag('span',$label,['class'=>$class]);
                                    },
                                    'headerOptions' => ['class'=>'text-center'],
                                    'contentOptions'=>['class'=>'text-center'],
                                ],


                            ],
                        ]);
                        Pjax::end();
                        ?>

                    </div>
                </div>
            </div>
        </div>

    </div>
</div>