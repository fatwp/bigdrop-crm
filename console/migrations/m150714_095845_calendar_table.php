<?php


class m150714_095845_calendar_table extends console\migrations\Migration
{

    public function up()
    {
        $this->createTable(self::TABLE_CALENDAR, [
            'id'            => 'INT UNSIGNED AUTO_INCREMENT PRIMARY KEY',
            'user_id'       => 'INT UNSIGNED NOT NULL',
            'type'          => 'TINYINT UNSIGNED NOT NULL',
            'title'         => 'VARCHAR(100) NOT NULL',
            'description'   => 'VARCHAR(255) NOT NULL',
            'start'         => "DATETIME NOT NULL",
            'end'           => "DATETIME DEFAULT NULL",
            'work_days'     => 'TINYINT UNSIGNED NULL',
            'status'        => "TINYINT UNSIGNED DEFAULT ".\common\models\Calendar::STATUS_AWAITING,
            'created_at'    => "DATETIME NOT NULL",
            'updated_at'    => "DATETIME DEFAULT NULL",

        ], $this->tableOptions);

        $this->addForeignKey(null,self::TABLE_CALENDAR,'user_id',self::TABLE_USER,'id','CASCADE','CASCADE');

    }

    public function down()
    {
        $this->dropTable(self::TABLE_CALENDAR);
    }

}
