<?php
namespace console\migrations;

abstract class Migration extends \yii\db\Migration {

    const TABLE_USER = '{{%user}}';
    const TABLE_PROFILE = '{{%profile}}';
    const TABLE_AUTH = '{{%auth}}';
    const TABLE_CALENDAR = '{{%calendar}}';


    public function getTableOptions(){
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {

            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        return $tableOptions;
    }

    public function addForeignKey($name, $table, $columns, $refTable, $refColumns, $delete = null, $update = null)
    {
        if(null === $name){
            $altColumns = $columns;
            if(is_array($altColumns)){
                $altColumns = implode('-',$altColumns);
            }

            $name = 'fk-'.trim($table,'{}%').'-'.$altColumns;
        }
        parent::addForeignKey($name, $table, $columns, $refTable, $refColumns, $delete, $update);
    }

    /**
     * Builds and executes a SQL statement for creating a new index.
     * @param string $name the name of the index. The name will be properly quoted by the method.
     * @param string $table the table that the new index will be created for. The table name will be properly quoted by the method.
     * @param string|array $columns the column(s) that should be included in the index. If there are multiple columns, please separate them
     * by commas or use an array. The column names will be properly quoted by the method.
     * @param boolean $unique whether to add UNIQUE constraint on the created index.
     */
    public function createIndex($name, $table, $columns, $unique = false)
    {
        if(null === $name){
            $altColumns = $columns;
            if(is_array($altColumns)){
                $altColumns = implode('-',$altColumns);
            }

            $name = 'idx-'.trim($table,'{}%').'-'.$altColumns;
        }
        parent::createIndex($name, $table, $columns, $unique);
    }


}