<?php

class m150420_174014_auth_table extends console\migrations\Migration
{
    public function up()
    {

        $this->createTable(self::TABLE_AUTH, [
            'id'                        => 'INT UNSIGNED AUTO_INCREMENT PRIMARY KEY',
            'user_id'                   => 'INT UNSIGNED NOT NULL',
            'source'                    => 'VARCHAR(255) NOT NULL',
            'source_id'                 => 'VARCHAR(255) NOT NULL',
            'created_at'                => "DATETIME NOT NULL",
            'updated_at'                => "DATETIME DEFAULT NULL",

        ], $this->tableOptions);

        $this->addForeignKey(null,self::TABLE_AUTH,'user_id',self::TABLE_USER,'id','CASCADE','CASCADE');
    }


    public function down()
    {
        $this->dropTable(self::TABLE_AUTH);
    }
    

}
