<?php



class m150416_165137_profile_table extends console\migrations\Migration
{
    public function up()
    {

        $this->createTable(self::TABLE_PROFILE, [
            'id'                        => 'INT UNSIGNED AUTO_INCREMENT PRIMARY KEY',
            'user_id'                   => 'INT UNSIGNED NOT NULL',
            'first_name'                => 'VARCHAR(30) DEFAULT NULL',
            'last_name'                 => 'VARCHAR(30) DEFAULT NULL',
            'gender'                    => 'CHAR(1) DEFAULT NULL',
            'position'                  => 'VARCHAR(30) DEFAULT NULL',
            'vacation_days'             => 'TINYINT UNSIGNED NULL',
            'employment_date'           => 'DATETIME DEFAULT NULL',
            'created_at'                => 'DATETIME NOT NULL',
            'updated_at'                => 'DATETIME DEFAULT NULL',
        ], $this->tableOptions);

        $this->createIndex(null,self::TABLE_PROFILE,'user_id',true);
        $this->addForeignKey(null,self::TABLE_PROFILE,'user_id',self::TABLE_USER,'id','CASCADE','CASCADE');
    }


    public function down()
    {
        $this->dropTable(self::TABLE_PROFILE);
    }
}
