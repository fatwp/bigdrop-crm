<?php



class m150409_164813_user_table extends console\migrations\Migration
{
    public function up()
    {


        $this->createTable(self::TABLE_USER, [
            'id'                        => "INT UNSIGNED AUTO_INCREMENT PRIMARY KEY",
            'username'                  => "VARCHAR(50) NOT NULL",
            'auth_key'                  => "VARCHAR(32) NOT NULL",
            'password_hash'             => "VARCHAR(255)NOT NULL",
            'password_reset_token'      => "VARCHAR(50) DEFAULT NULL",
            'email'                     => "VARCHAR(127)NOT NULL",
            'email_confirmation_token'  => "VARCHAR(50) DEFAULT NULL",
            'status'                    => "TINYINT UNSIGNED DEFAULT ".\common\interfaces\IStatus::STATUS_PENDING,
            'role'                      => "TINYINT UNSIGNED DEFAULT ".\frontend\models\User::ROLE_USER,
            'check_in'                  => "DATE DEFAULT NULL",
            'created_at'                => "DATETIME NOT NULL",
            'updated_at'                => "DATETIME DEFAULT NULL",

        ], $this->tableOptions);

        $user = new \frontend\models\User([
            'username' => 'admin',
            'role' => \frontend\models\User::ROLE_ADMIN,
            'email' => 'crm@bigdropinc.com',
            'password' => 'bigdrop',
            'status'=> \frontend\models\User::STATUS_ACTIVE,
        ]);

        $user->generateAuthKey();
        $user->generatePasswordResetToken();
        if(!$user->save()){
            throw new Exception(\yii\helpers\Json::encode($user->getErrors()));
        }
    }

    public function down()
    {
        $this->dropTable(self::TABLE_USER);
    }
    

}
