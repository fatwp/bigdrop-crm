<?php
/**
 * Created by PhpStorm.
 * User: buba
 * Date: 8/4/15
 * Time: 10:09 AM
 */

namespace console\controllers;


use common\helpers\DateHelper;
use common\models\Calendar;
use common\models\Profile;
use console\migrations\Migration;
use yii\console\Controller;

class TaskController extends Controller
{
    public function actionWorkDays()
    {
        $holidays = (new \yii\db\Query())
            ->where(['status' => Calendar::STATUS_APPROVED, 'type' => Calendar::TYPE_HOLIDAY])
            ->select(['start','end'])
            ->from(Migration::TABLE_CALENDAR)
            ->all();
        $calendar = (new \yii\db\Query())
            ->where('work_days IS NULL')
            ->select(['id','user_id','start','end','type'])
            ->from(Migration::TABLE_CALENDAR);

        $users = [];
        foreach($calendar->each() as $item){
            if($item['type'] == Calendar::TYPE_HOLIDAY){
                Calendar::updateAll(
                    ['work_days' => DateHelper::getWorkingDays($item['start'],$item['end'])],
                    ['id'=>$item['id']]
                );
            }else{
                Calendar::updateAll(
                    ['work_days' => DateHelper::getWorkingDays($item['start'],$item['end'],$holidays)],
                    ['id'=>$item['id']]
                );

                if(!in_array($item['user_id'],$users)){
                    $users[] = $item['user_id'];
                }
            }

        }

        if($users){
            $users = implode(',',$users);
            $table_profile = Migration::TABLE_PROFILE;
            $table_calendar = Migration::TABLE_CALENDAR;
            $subSQl = "SELECT SUM([[work_days]]) FROM $table_calendar "
                     ."WHERE $table_profile.[[user_id]]= $table_calendar.[[user_id]] AND status=:status AND type=:type";
            $sql = "UPDATE $table_profile SET [[vacation_days]] = ($subSQl) WHERE user_id IN ($users)";
            Profile::getDb()
                ->createCommand($sql)
                ->bindValues([':status' => Calendar::STATUS_APPROVED, ':type' => Calendar::TYPE_VACATION])
                ->execute();
        }

        $this->stdout('calendar work days setting successful');
        $this->stdout(PHP_EOL);
    }
}