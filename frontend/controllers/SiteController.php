<?php
namespace frontend\controllers;

use common\models\Auth;
use common\models\Calendar;
use common\models\Profile;
use frontend\models\ContactForm;
use frontend\models\UserSettingForm;
use frontend\models\UserSignUpForm;
use frontend\models\User;
use Yii;
use frontend\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\CalendarForm;
use yii\base\InvalidParamException;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\ServerErrorHttpException;
use yii\widgets\ActiveForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'sign-up','login','request-password-reset','index'],
                'rules' => [
                    [
                        'actions' => ['sign-up','login','request-password-reset'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout','index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'onAuthSuccess'],
                'successUrl' => Url::to(['site/index']),
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionHoliday()
    {
        /**@var $user User*/
        $user = Yii::$app->user->identity;

        if($user->role < User::ROLE_ADMIN){
            throw new ForbiddenHttpException();
        }

        $model = new CalendarForm(['scenario'=>'holiday']);
        $model->id = Yii::$app->request->get('edit');

        if($model->id){
            /**@var $calendar Calendar*/
            $calendar = Calendar::findOne(['id'=>$model->id]);
            if(!$calendar){
                throw new NotFoundHttpException();
            }
            $model->attributes = $calendar->attributes;
            $model->start = (new \DateTime($model->start))->format('d-m-Y');
            $model->end = (new \DateTime($model->end))->format('d-m-Y');
        }

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->save()) {
            Yii::$app->getSession()->setFlash('success', $model->id?
                'Your changes are successfully saved':'New holiday has been successfully added');
            return Yii::$app->getResponse()->redirect(['site/holidays']);
        }else if (Yii::$app->request->isPost){
            Yii::$app->getSession()->setFlash('error',  $model->id?
                'We were unable to save changes':'We were unable to add the new holiday');
        }
        return $this->render('holiday',['model'=>$model]);
    }

    /**
     * @param $client \yii\authclient\ClientInterface
     * @return Response | null;
     */
    public function onAuthSuccess($client)
    {
        /**@var $auth \common\models\Auth */

        $attributes = Auth::parseAttributes($client);
        $auth = Auth::find()->where([
            'source' => $client->getId(),
            'source_id' => $attributes['id'],
        ])->one();

        if (Yii::$app->user->isGuest) {
            if ($auth) { // login
                $user = $auth->user;
                Yii::$app->user->login($user);
            } else { // signup
                if (isset($attributes['email'])  && User::find()->where(['email' => $attributes['email']])->exists()) {
                    Yii::$app->getSession()->setFlash('error',
                        Yii::t('app', "User with the same email as in {client} account already exists but isn't linked to it. Login using email first to link it.", ['client' => $client->getTitle()])
                    );
                } else {
                    $attributes['sourceName']   = $client->getId();
                    $attributes['sourceId']     = $attributes['id'];
                    $response = Yii::$app->getResponse();
                    $response->content = Yii::$app->getView()->renderFile(Yii::getAlias('@app/views/site/auth/popup.php'),['attributes'=>$attributes]);
                    return $response;
                }
            }
        } else { // user already logged in
            if (!$auth) { // add auth provider
                $auth = new Auth([
                    'user_id' => Yii::$app->user->id,
                    'source' => $client->getId(),
                    'source_id' => $attributes['id'],
                ]);
                $auth->save();
            }
        }

        return null;

    }

    public function actionLogin()
    {
        $this->layout = 'auth';
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack(['site/index']);
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return Yii::$app->getResponse()->redirect(['site/login']);
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionSignUp()
    {
        $this->layout = 'auth';
        $model = new UserSignUpForm();
        $model->gender = Profile::GENDER_MALE;
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signUp()) {
                if (Yii::$app->user->login($user)) {
                    return $this->goHome();

                }else{
                    return Yii::$app->getResponse()->redirect(['site/login']);
                }
            }
        }

        return $this->render('sign_up', [
            'model' => $model,
        ]);
    }

    public function actionRequestPasswordReset()
    {
        $this->layout = 'auth';
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->getSession()->setFlash('success', 'Check your email for further instructions.');
                $model->email = null;

            } else {
                Yii::$app->getSession()->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    public function actionResetPassword()
    {
        $token = Yii::$app->request->get('token');
        $this->layout = 'auth';
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->getSession()->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function actionVacation()
    {
        /**@var $user User*/
        $user = Yii::$app->user->identity;

        if($user->role <= User::ROLE_USER){
            throw new ForbiddenHttpException();
        }

        $model = new CalendarForm(['scenario'=>'vacation']);
        $model->id = Yii::$app->request->get('edit');


        if($model->id){
            /**@var $calendar Calendar*/
            $calendar = Calendar::findOne(['id'=>$model->id]);
            if(!$calendar){
                throw new NotFoundHttpException();
            }

            if($user->role < User::ROLE_PM){
                throw new ForbiddenHttpException();
            }

            $model->attributes = $calendar->attributes;
            $model->start = (new \DateTime($model->start))->format('d-m-Y');
            $model->end = (new \DateTime($model->end))->format('d-m-Y');
        }else{

            if($user->role < User::ROLE_ADMIN){
                $model->username = $user->username;
                $model->title  = $user->username.'\'s vacation';
            }
        }

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->save()) {
            Yii::$app->getSession()->setFlash('success', $model->id?
                'Your changes are successfully saved':'New vacation has been successfully added');
            return Yii::$app->getResponse()->redirect(['site/vacations']);
        }else if (Yii::$app->request->isPost){
            Yii::$app->getSession()->setFlash('error',  $model->id?
                'We were unable to save changes':'We were unable to add the new vacation');
        }
        return $this->render('vacation',['model'=>$model]);
    }

    public function actionUser()
    {
        /**@var $user User*/
        $user = Yii::$app->user->identity;


        if($user->role <= User::ROLE_PM){
            throw new ForbiddenHttpException();
        }

        $model = new UserSettingForm();
        $model->id = Yii::$app->request->get('edit');
        $model->gender = Profile::GENDER_MALE;

        if($model->id){
            /**@var $user User*/
            $user = User::findIdentity($model->id);
            if(!$user){
                throw new NotFoundHttpException();
            }


            $model->attributes = ArrayHelper::merge($user->profile->attributes,$user->attributes);
            $model->firstName = $user->profile->first_name;
            $model->lastName = $user->profile->last_name;

            $model->employment_date = $model->employment_date?
                (new \DateTime($model->employment_date))->format('d-m-Y'):null;

        }

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->save()) {
            Yii::$app->getSession()->setFlash('success', $model->id?
                'Your changes are successfully saved':'New member has been successfully added');
            return Yii::$app->getResponse()->redirect(['site/users']);
        }else if (Yii::$app->request->isPost){
            Yii::$app->getSession()->setFlash('error',  $model->id?
                'We were unable to save changes':'We were unable to add the new member');
        }
        return $this->render('user',['model'=>$model]);
    }

    public function actionUsers(){

        if(Yii::$app->request->isAjax && Yii::$app->request->get('update')){
            $name = Yii::$app->request->get('name');
            $pk     = Yii::$app->request->get('pk');
            $value  = Yii::$app->request->get('value');
            /**@var $user User */
            $user = User::findIdentity($pk);
            if($user){

                try {
                    $user->setAttribute($name, $value);
                } catch (\Exception $e) {
                    return ['status'=>'error','message'=>$e->getMessage()];
                }

                Yii::$app->response->format = Response::FORMAT_JSON;
                if($user->save()){
                    return ['status'=>'success','message'=>"update successful"];
                }else{
                    return ['status'=>'error','message'=>"cannot save changes"];
                }
            }

            throw new BadRequestHttpException();

        }

        if(Yii::$app->request->isAjax && Yii::$app->request->get('delete')){
            /**@var $user User */
            $user = User::findOne(['id'=>Yii::$app->request->get('delete')]);
            if(!$user){
                throw new BadRequestHttpException();
            }

            if($user->id == Yii::$app->user->id){
                throw new ForbiddenHttpException('You are attempting to delete your self');
            }

            if(!$user->delete()){
                throw new ServerErrorHttpException();
            }
        }

        /** @var $query \yii\db\ActiveQuery */
        $model = new User(['scenario' =>'search']);
        $model->load(Yii::$app->request->get());
        $query = null;
        $query = User::find()->andWhere(['<>','role',User::ROLE_ADMIN])->joinWith('profile',true,'INNER JOIN')->select(['user.*',"CONCAT_WS(' ',first_name,last_name) AS name, vacation_days"]);

        $query->andFilterWhere(['role'=>$model->role]);
        $query->andFilterWhere(['status'=>$model->status]);
        $query->andFilterWhere(['like' ,"CONCAT_WS(' ',first_name,last_name)",'%'.strtr($model->fullName,['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']).'%',false]);

        $provider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 5,
            ],
            'sort'=>[
                'defaultOrder'=>[
                    'created_at' => SORT_DESC,
                ],
                'attributes' => [
                    'email',
                    'created_at',
                    'name' => [
                        'asc' => ['first_name' => SORT_ASC, 'last_name' => SORT_ASC],
                        'desc' => ['first_name' => SORT_DESC, 'last_name' => SORT_DESC],
                        'label' => 'Name',
                    ],
                ],
            ]
        ]);

        return $this->render('users',['dataProvider'=>$provider,'model'=>$model]);
    }

    public function actionAdmins(){

        if(Yii::$app->request->isAjax && Yii::$app->request->get('update')){
            $name = Yii::$app->request->get('name');
            $pk     = Yii::$app->request->get('pk');
            $value  = Yii::$app->request->get('value');
            /**@var $user User */
            $user = User::findIdentity($pk);
            if($user){

                try {
                    $user->setAttribute($name, $value);
                } catch (\Exception $e) {
                    return ['status'=>'error','message'=>$e->getMessage()];
                }

                Yii::$app->response->format = Response::FORMAT_JSON;
                if($user->save()){
                    return ['status'=>'success','message'=>"update successful"];
                }else{
                    return ['status'=>'error','message'=>"cannot save changes"];
                }
            }

            throw new BadRequestHttpException();

        }

        if(Yii::$app->request->isAjax && Yii::$app->request->get('delete')){
            /**@var $user User */
            $user = User::findOne(['id'=>Yii::$app->request->get('delete')]);
            if(!$user){
                throw new BadRequestHttpException();
            }

            if($user->id == Yii::$app->user->id){
                throw new ForbiddenHttpException('You are attempting to delete your self');
            }

            if(!$user->delete()){
                throw new ServerErrorHttpException();
            }
        }

        /** @var $query \yii\db\ActiveQuery */
        $model = new User(['scenario' =>'search']);
        $model->load(Yii::$app->request->get());
        $query = null;
        $query = User::find()->andWhere(['=','role',User::ROLE_ADMIN])->joinWith('profile',true,'INNER JOIN')->select(['user.*',"CONCAT_WS(' ',first_name,last_name) AS name"]);

        $query->andFilterWhere(['role'=>$model->role]);
        $query->andFilterWhere(['user.status'=>$model->status]);
        $query->andFilterWhere(['like' ,"CONCAT_WS(' ',first_name,last_name)",'%'.strtr($model->fullName,['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']).'%',false]);

        $provider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 5,
            ],
            'sort'=>[
                'defaultOrder'=>[
                    'created_at' => SORT_DESC,
                ],
                'attributes' => [
                    'email',
                    'created_at',
                    'name' => [
                        'asc' => ['first_name' => SORT_ASC, 'last_name' => SORT_ASC],
                        'desc' => ['first_name' => SORT_DESC, 'last_name' => SORT_DESC],
                        'label' => 'Name',
                    ],
                ],
            ]
        ]);

        return $this->render('admins',['dataProvider'=>$provider,'model'=>$model]);
    }

    public function actionVacations(){

        if(Yii::$app->request->isAjax && Yii::$app->request->get('update')){
            $name = Yii::$app->request->get('name');
            $pk     = Yii::$app->request->get('pk');
            $value  = Yii::$app->request->get('value');
            /**@var $user Calendar */
            $calendar = Calendar::find()->where(['id'=>$pk])->one();
            if($calendar){
                try {
                    $calendar->setAttribute($name, $value);
                } catch (\Exception $e) {
                    return ['status'=>'error','message'=>$e->getMessage()];
                }
                Yii::$app->response->format = Response::FORMAT_JSON;
                if($calendar->save()){
                    return ['status'=>'success','message'=>"update successful"];
                }else{
                    return ['status'=>'error','message'=>"cannot save changes"];
                }
            }

            throw new BadRequestHttpException();

        }

        if(Yii::$app->request->isAjax && Yii::$app->request->get('delete')){
            /**@var $calendar Calendar */
            $calendar = Calendar::findOne(['id'=>Yii::$app->request->get('delete')]);
            if(!$calendar){
                throw new BadRequestHttpException();
            }
            if(!$calendar->delete()){
                throw new ServerErrorHttpException();
            }
        }

        /** @var $query \yii\db\ActiveQuery */
        $model = new Calendar(['scenario' =>'search']);
        $model->load(Yii::$app->request->get());
        $query = null;
        $query = Calendar::find()->andWhere(['type'=>Calendar::TYPE_VACATION])->joinWith('user',true,'INNER JOIN')->select(['calendar.*','username']);

        $query->andFilterWhere(['calendar.status'=>$model->status]);
        $query->andFilterWhere(['like' ,'username','%'.strtr($model->username,['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']).'%',false]);

        $provider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 5,
            ],
            'sort'=>[
                'defaultOrder'=>[
                    'created_at' => SORT_DESC,
                ],
                'attributes' => [
                    'start',
                    'end',
                    'created_at',
                    'username' => [
                        'asc' => ['username' => SORT_ASC],
                        'desc' => ['username' => SORT_DESC],
                        'label' => 'Username',
                    ],
                ],
            ]
        ]);

        return $this->render('vacations',['dataProvider'=>$provider,'model'=>$model]);
    }

    public function actionHolidays(){

        if(Yii::$app->request->isAjax && Yii::$app->request->get('update')){
            Yii::$app->response->format = Response::FORMAT_JSON;

            $name = Yii::$app->request->get('name');
            $pk     = Yii::$app->request->get('pk');
            $value  = Yii::$app->request->get('value');
            /**@var $calendar Calendar */
            $calendar = Calendar::find()->where(['id'=>$pk])->one();
            if($calendar){

                $tr = Calendar::getDb()->beginTransaction();
                try {
                    $calendar->setAttribute($name, $value);
                    $tr->commit();
                } catch (\Exception $e) {
                    $tr->rollBack();
                    return ['status'=>'error','message'=>$e->getMessage()];
                }

                if($calendar->save()){
                    return ['status'=>'success','message'=>"update successful"];
                }else{
                    return ['status'=>'error','message'=>"cannot save changes"];
                }
            }

            throw new BadRequestHttpException();

        }

        if(Yii::$app->request->isAjax && Yii::$app->request->get('delete')){
            Yii::$app->response->format = Response::FORMAT_JSON;
            /**@var $calendar Calendar */
            $calendar = Calendar::findOne(['id'=>Yii::$app->request->get('delete')]);
            if(!$calendar){
                throw new BadRequestHttpException();
            }

            $tr = Calendar::getDb()->beginTransaction();
            try {
                if(!$calendar->delete()){
                    throw new ServerErrorHttpException();
                }
                $tr->commit();

            } catch (\Exception $e) {
                $tr->rollBack();
                return ['status'=>'error','message'=>$e->getMessage()];
            }

        }

        /** @var $query \yii\db\ActiveQuery */
        $model = new Calendar(['scenario' =>'search']);
        $model->load(Yii::$app->request->get());
        $query = null;
        $query = Calendar::find()->andWhere(['type'=>Calendar::TYPE_HOLIDAY])->joinWith('user',true,'INNER JOIN')->select(['calendar.*','username']);

        $query->andFilterWhere(['calendar.status'=>$model->status]);
        $query->andFilterWhere(['like' ,'username','%'.strtr($model->username,['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']).'%',false]);

        $provider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 5,
            ],
            'sort'=>[
                'defaultOrder'=>[
                    'created_at' => SORT_DESC,
                ],
                'attributes' => [
                    'start',
                    'end',
                    'created_at',
                    'username' => [
                        'asc' => ['username' => SORT_ASC],
                        'desc' => ['username' => SORT_DESC],
                        'label' => 'Username',
                    ],
                ],
            ]
        ]);

        return $this->render('holidays',['dataProvider'=>$provider,'model'=>$model]);
    }

    public function actionMembers(){
        $query = Yii::$app->request->get('query');
        Yii::$app->response->format = Response::FORMAT_JSON;
        $members =  User::find()
            ->joinWith('profile',true,'INNER JOIN')
            ->where([
                    'or',
                    ['like','profile.first_name',$query],
                    ['like','profile.last_name',$query],
                    ['like','profile.position',$query],
                    ['like','user.username',$query],
                    ['like','user.email',$query],
                    ['like','user.role',$query],
            ])
            ->andWhere(['<>','role',User::ROLE_ADMIN])
            ->select([
                'profile.first_name',
                'profile.last_name',
                'profile.position',
                'user.username',
                'user.email',
                'user.role',
                'user.id',
            ])
            ->limit(5)
            ->asArray()
            ->all();

        return array_map(
            function($member) {
                $item = [];
                $item['name'] = $member['first_name'].' '.$member['last_name'];
                $item['value'] = $member['username'];
                $item['description'] = $member['position'];
                $item['label'] = User::roleList()[intval($member['role'])];
                return $item;
            },
            $members
        );

    }
}
