<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'name'=>'Big Drop CRM',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'homeUrl'=>'/',
    'components' => [
        'user' => [
            'identityClass' => 'frontend\models\User',
            'enableAutoLogin' => true,
        ],
        'request'=>[
            'baseUrl'=>'',
        ],
        'assetManager' => [
            'bundles' => [
                'yii\web\JqueryAsset' => false,
                'yii\bootstrap\BootstrapAsset' => false,
                'yii\bootstrap\BootstrapPluginAsset' => false,
                'dosamigos\fileupload\FileUploadPlusAsset' => [
                    'css' => []
                ],
                'dosamigos\fileupload\FileUploadAsset' => [
                    'css' => []
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
                ''  =>  'site/index',
                'login' =>  'site/login',
                'logout' =>  'site/logout',
                'sign-up'  =>  'site/sign-up',
                'forgot-password'  =>  'site/request-password-reset',
                'reset-password'  =>  'site/reset-password',
                'auth'  =>  'site/auth',
                'user/edit/<edit:\d+>'  =>  'site/user',
                'users/delete/<delete:\d+>'  =>  'site/users',
                'user/add'  =>  'site/user',
                'users'  =>  'site/users',
                'admins'  =>  'site/admins',
                'vacation/edit/<edit:\d+>'  =>  'site/vacation',
                'vacation/delete/<delete:\d+>'  =>  'site/vacations',
                'vacation/add'  =>  'site/vacation',
                'vacations'  =>  'site/vacations',
                'holiday/edit/<edit:\d+>'  =>  'site/holiday',
                'holiday/delete/<delete:\d+>'  =>  'site/holidays',
                'holiday/add'  =>  'site/holiday',
                'holidays'  =>  'site/holidays',
                '<username:[[a-z][0-9A-za-z_]{2,10}>'=>'user/profile',
                '<username:[[a-z][0-9A-za-z_]{2,10}>/<action:^(profile)>'=>'user/<action>',
                [
                    'pattern' => 'members',
                    'route' => 'site/members',
                    'suffix' => '.json',
                ]
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
    ],
    'params' => $params,
];
