<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class EditableAsset extends AssetBundle
{
    public function init()
    {
        $this->basePath = '@webroot';
        $this->baseUrl  = '@web';
        $this->css      = [
            'css/libs/select2.css',
            'css/libs/bootstrap-editable.css',
        ];
        $this->js       = [
            'js/bootstrap-editable.min.js',
            'js/select2.min.js',
        ];
        parent::init();
    }
}
