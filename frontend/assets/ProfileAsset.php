<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ProfileAsset extends AssetBundle
{

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/libs/magnific-popup.css',
    ];
    public $js = [
        'js/jquery.slimscroll.min.js',
        'js/jquery.magnific-popup.min.js',
        'js/scripts.js',
        'js/pace.min.js'
    ];
    public $depends = [
        '\frontend\assets\BaseAsset'
    ];
}
