<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user \frontend\models\User */
$this->title = 'Profile';
\frontend\assets\ProfileAsset::register($this);
$script=<<<INLINE
    $(document).ready(function() {
        $('.conversation-inner').slimScroll({
            height: '340px'
        });
    });

    $(function() {
        $(document).ready(function() {
            $('#newsfeed .story-images').magnificPopup({
                type: 'image',
                delegate: 'a',
                gallery: {
                    enabled: true
                }
            });
        });
    });
INLINE;

$this->registerJs($script);
?>
<div class="row">
    <div class="col-lg-12">

        <div class="row">
            <div class="col-lg-12">

                <h1><?=Yii::$app->user->id == $user->id?'My profile':$user->username.'\' profile'?></h1>
                <?php foreach(Yii::$app->session->getAllFlashes(true) as $key=>$val):?>
                <div class="alert alert-<?=$key?> fade in">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?=$val?>
                </div>
                <?php endforeach;?>
            </div>
        </div>

        <div class="row" id="user-profile">
            <div class="col-lg-3 col-md-4 col-sm-4">
                <div class="main-box clearfix">
                    <header class="main-box-header clearfix">
                        <h2><?=$user->getFullName()?:$user->username?></h2>
                    </header>

                    <div class="main-box-body clearfix">

                        <img src="<?=$user->profile->getAvatar('medium')?>" alt="" class="profile-img img-responsive center-block" />

                        <div class="profile-label">
                            <span class="label label-default" ><?=$user->getRole()?></span>
                        </div>

                        <div class="profile-label">
                            <span ><?=$user->profile->position?></span>
                        </div>

                        <div class="profile-since">
                            Member since: <?=Yii::$app->formatter->asDate($user->created_at,'php:F d, Y')?>
                        </div>


                        <?php if(null !== $user->email_confirmation_token && Yii::$app->user->id == $user->id):?>
                            <div class="profile-message-btn center-block text-center">
                                <a href="<?=Yii::$app->urlManager->createUrl(['user/confirm-email-request']);?>" >
                                    Confirm your email
                                </a>
                            </div>
                        <?php endif;?>
                    </div>
                </div>

            </div>

            <div class="col-lg-9 col-md-8 col-sm-8">
                <div class="main-box clearfix">
                    <div class="tabs-wrapper profile-tabs">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab-newsfeed" data-toggle="tab">Newsfeed</a></li>
                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="tab-newsfeed">

                                <div id="newsfeed">
                                    <div class="story">
                                        <div class="story-user">
                                            <a href="#">
                                                <img src="/img/samples/robert-300.jpg" alt=""/>
                                            </a>
                                        </div>

                                        <div class="story-content">
                                            <header class="story-header">
                                                <div class="story-author">
                                                    <a href="#" class="story-author-link">
                                                        Robert Downey Jr.
                                                    </a>
                                                    posted a status update
                                                </div>
                                                <div class="story-time">
                                                    <i class="fa fa-clock-o"></i> just now
                                                </div>
                                            </header>
                                            <div class="story-inner-content">
                                                Now that we know who you are, I know who I am. I'm not a mistake!
                                                It all makes sense! In a comic, you know how you can tell who the
                                                arch-villain's going to be? He's the exact opposite of the hero.
                                                And most times they're friends, like you and me! I should've known
                                                way back when... You know why, David? Because of the kids.
                                                They called me Mr Glass.
                                            </div>
                                            <footer class="story-footer">
                                                <a href="#" class="story-comments-link">
                                                    <i class="fa fa-comment fa-lg"></i> 8320 Comments
                                                </a>
                                                <a href="#" class="story-likes-link">
                                                    <i class="fa fa-heart fa-lg"></i> 82k Likes
                                                </a>
                                            </footer>
                                        </div>
                                    </div>

                                    <div class="story">
                                        <div class="story-user">
                                            <a href="#">
                                                <img src="/img/samples/angelina-300.jpg" alt=""/>
                                            </a>
                                        </div>

                                        <div class="story-content">
                                            <header class="story-header">
                                                <div class="story-author">
                                                    <a href="#" class="story-author-link">
                                                        Angelina Jolie
                                                    </a>
                                                    checked in at <a href="#">Place du Casino</a>
                                                </div>
                                                <div class="story-time">
                                                    <i class="fa fa-clock-o"></i> 3 Minutes ago
                                                </div>
                                            </header>
                                            <div class="story-inner-content">
                                                <div id="map-apple" class="map-content"></div>
                                            </div>
                                            <footer class="story-footer">
                                                <a href="#" class="story-comments-link">
                                                    <i class="fa fa-comment fa-lg"></i> 23k Comments
                                                </a>
                                                <a href="#" class="story-likes-link">
                                                    <i class="fa fa-heart fa-lg"></i> 159k Likes
                                                </a>
                                            </footer>
                                        </div>
                                    </div>

                                    <div class="story">
                                        <div class="story-user">
                                            <a href="#">
                                                <img src="/img/samples/ryan-300.jpg" alt=""/>
                                            </a>
                                        </div>

                                        <div class="story-content">
                                            <header class="story-header">
                                                <div class="story-author">
                                                    <a href="#" class="story-author-link">
                                                        Ryan Gossling
                                                    </a>
                                                    uploaded 5 new photos to album <a href="#">Bora Bora</a>
                                                </div>
                                                <div class="story-time">
                                                    <i class="fa fa-clock-o"></i> 8 Hours ago
                                                </div>
                                            </header>
                                            <div class="story-inner-content">
                                                <div class="story-images clearfix">
                                                    <a href="/img/samples/tahiti-1.jpg" class="story-image-link">
                                                        <img src="/img/samples/tahiti-1.jpg" alt="" class="img-responsive"/>
                                                    </a>
                                                    <a href="/img/samples/tahiti-2.jpg" class="story-image-link story-image-link-small">
                                                        <img src="/img/samples/tahiti-2.jpg" alt="" class="img-responsive"/>
                                                    </a>
                                                    <a href="/img/samples/tahiti-3.jpg" class="story-image-link story-image-link-small">
                                                        <img src="/img/samples/tahiti-3.jpg" alt="" class="img-responsive"/>
                                                    </a>
                                                    <a href="/img/samples/tahiti-3.jpg" class="story-image-link story-image-link-small">
                                                        <img src="/img/samples/tahiti-3.jpg" alt="" class="img-responsive"/>
                                                    </a>
                                                    <a href="/img/samples/tahiti-2.jpg" class="story-image-link story-image-link-small hidden-xs">
                                                        <img src="/img/samples/tahiti-2.jpg" alt="" class="img-responsive"/>
                                                    </a>
                                                </div>
                                            </div>
                                            <footer class="story-footer">
                                                <a href="#" class="story-comments-link">
                                                    <i class="fa fa-comment fa-lg"></i> 46 Comments
                                                </a>
                                                <a href="#" class="story-likes-link">
                                                    <i class="fa fa-heart fa-lg"></i> 823 Likes
                                                </a>
                                            </footer>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>


        </div>

    </div>
</div>
