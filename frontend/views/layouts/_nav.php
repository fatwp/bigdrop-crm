<div id="nav-col">
<section id="col-left" class="col-left-nano">
<div id="col-left-inner" class="col-left-nano-content">
<div class="collapse navbar-collapse navbar-ex1-collapse" id="sidebar-nav">
<ul class="nav nav-pills nav-stacked">
<li class="<?=Yii::$app->controller->action->getUniqueId()=='site/index'?'active':''?>">
    <a href="<?=\yii\helpers\Url::base()?>/" >
        <i class="fa fa-dashboard"></i>
        <span>Dashboard</span>
    </a>
</li>
<li class="<?=in_array(Yii::$app->controller->action->getUniqueId(),['site/admins','site/users'])?'open active':'';?>">
    <a href="#" class="dropdown-toggle">
        <i class="fa fa-users"></i>
        <span>Users</span>
        <i class="fa fa-chevron-circle-right drop-icon"></i>
    </a>
    <ul class="submenu">
        <li>
            <a href="<?=\yii\helpers\Url::to(['site/admins'])?>" class="<?=Yii::$app->controller->action->getUniqueId()=='site/admins'?'active':''?>">
                Administrators
            </a>
        </li>
        <li>
            <a href="<?=\yii\helpers\Url::to(['site/users'])?>" class="<?=Yii::$app->controller->action->getUniqueId()=='site/users'?'active':''?>">
                Members
            </a>
        </li>
    </ul>
</li>
    <li class="<?=Yii::$app->controller->action->getUniqueId()=='site/vacations'?'active':''?>">
        <a href="<?=\yii\helpers\Url::to(['site/vacations'])?>" >
            <i class="fa fa-plane"></i>
            <span>Vacations</span>
        </a>
    </li>
    <li class="<?=Yii::$app->controller->action->getUniqueId()=='site/holidays'?'active':''?>">
        <a href="<?=\yii\helpers\Url::to(['site/holidays'])?>" >
            <i class="fa fa-h-square"></i>
            <span>Holidays</span>
        </a>
    </li>
</ul>
</div>
</div>
</section>
</div>