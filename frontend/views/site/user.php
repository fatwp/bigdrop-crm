<?php
/**@var $model \frontend\models\UserSettingForm */
/* @var $this yii\web\View */
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

\frontend\assets\BaseAsset::register($this);
\frontend\assets\DatePickerAsset::register($this);
\frontend\assets\PasswordStrengthAsset::register($this);
$pwd = Html::getInputId($model,'password');
$script = <<<JS
    $('.date-picker').datepicker({
            format: 'dd-mm-yyyy'
    });

    /*$('#$pwd').pwstrength({
        label: '.pwdstrength-label'
    });*/
JS;
$this->registerJs($script,\yii\web\View::POS_END);
$this->title = 'User | '.Yii::$app->name;
?>
<div class="row">
    <div class="col-lg-4">

    </div>
    <div class="col-lg-4">
        <div class="main-box">
            <header class="main-box-header clearfix">
                <h2>Member</h2>
            </header>

            <div class="main-box-body clearfix">
                <?php ?>
                <?php $form = ActiveForm::begin(
                    [
                        'id' => 'holiday-form',
                        'enableAjaxValidation' => true,
                        'validateOnSubmit' => true,
                        'validateOnChange' => false,
                        'validateOnBlur' => false,
                    ]
                );  ?>

                <?= $form->field($model, 'username',['template'=>"<div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-user\"></i></span>{input}</div>{error}"])->textInput(['placeholder'=>'Username']) ?>
                <?= $form->field($model, 'email',['template'=>"<div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-envelope-o\"></i></span>{input}</div>{error}"])->textInput(['placeholder'=>'Email address']) ?>
                <?= $form->field($model, 'password',['template'=>"<div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-lock\"></i></span>{input}</div><!--<div id=\"pwindicator\" class=\"pwdindicator\"><div class=\"bar\"></div> <div class=\"pwdstrength-label\"></div> </div>-->{error}"])->passwordInput(['placeholder'=>'Enter password','data-indicator'=>'pwindicator']) ?>
                <?= $form->field($model, 'confirmPassword',['template'=>"<div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-unlock\"></i></span>{input}</div>{error}"])->passwordInput(['placeholder'=>'Re-enter password']) ?>
                <?= $form->field($model, 'firstName',['template'=>"<div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa\">F</i></span>{input}</div>{error}"])->textInput(['placeholder'=>'First name']) ?>
                <?= $form->field($model, 'lastName',['template'=>"<div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa\">L</i></span>{input}</div>{error}"])->textInput(['placeholder'=>'Last name']) ?>
                <?= $form->field($model, 'position',['template'=>"<div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa\">P</i></span>{input}</div>{error}"])->textInput(['placeholder'=>'Position']) ?>
                <?= $form->field($model, 'role')->dropDownList(\frontend\models\User::roleList())->label(false)?>
                <?= $form->field($model, 'employment_date',['template'=>"<div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-check-square-o\"></i></span>{input}</div>{error}"])->textInput(['placeholder'=>'Date Of Employment','class'=>'form-control date-picker']) ?>
                <!--<?/*= $form->field($model, 'vacation_days',['template'=>"<div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-plane\"></i></span>{input}</div>{error}"])->textInput(['placeholder'=>'Vacation Days','type'=>'number']) */?>-->
                <!--<?/*= $form->field($model, 'status')->dropDownList(\yii\helpers\ArrayHelper::getColumn(\frontend\models\User::statusList(),'text'))->label(false)*/?>-->
                <div class="row form-group">
                <?= $form->field($model, 'status',['options'=>['class'=>'col-xs-12'],'wrapperOptions'=>['class'=>'btn-group col-xs-12','data-toggle'=>'buttons']])->radioList(
                    \yii\helpers\ArrayHelper::getColumn(\frontend\models\User::statusList(),'text'),
                    [
                        'item'=>function ($index, $label, $name, $checked, $value) {
                            return '<label class="btn btn-'.(\frontend\models\User::statusList()[$value]['suffix']).' col-xs-3 '.($checked?'active':'').'">' . Html::input('radio',$name, $value, ['checked' => $checked]).$label.'</label>';
                        },
                        'class'=>'btn-group col-xs-12','data-toggle'=>'buttons',
                    ]
                )
                    ->label(false) ?>
                </div>
                <?= $form->field($model, 'gender')->radioList(
                    [
                        \common\models\Profile::GENDER_MALE=>'Male',
                        \common\models\Profile::GENDER_FEMALE=>'Female'
                    ],
                    [
                        'item'=>function ($index, $label, $name, $checked, $value) {
                            return '<div class="radio">'.Html::input('radio',$name, $value, ['checked' => $checked,'id'=>'radio'.$index]).'<label for="radio'.$index.'">'.$label.'</label></div>';
                        },

                    ]
                ) ?>



                <div class="row">
                    <div class="col-xs-12">
                        <?= Html::submitButton(Yii::$app->request->get('edit')?'Save':'Add', ['class' => 'btn btn-success col-xs-12', 'name' => 'sign-up-button']) ?>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
    <div class="col-lg-4">

    </div>
</div>