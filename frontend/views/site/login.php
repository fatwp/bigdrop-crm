<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Sign in | '.Yii::$app->name;
$this->params['breadcrumbs'][] = $this->title;

$script=<<<INLINE
    document.addEventListener('auth', function (e) {
        $('#login-box-inner .alert-danger').show();
    }, false);
INLINE;

$this->registerJs($script);
\frontend\assets\BaseAsset::register($this);
?>


<div class="row">
    <div class="col-xs-12">
        <div id="login-box">
            <div id="login-box-holder">
                <div class="row">
                    <div class="col-xs-12">
                        <header id="login-header">
                            <div id="login-logo">
                                <img src="img/logo.png" alt=""/>
                            </div>
                        </header>
                        <div id="login-box-inner">
                            <?php if(Yii::$app->session->hasFlash('error')):?>
                                <div class="alert alert-danger fade in">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <i class="fa fa-times-circle fa-fw fa-lg"></i>
                                    <strong>Oh snap!</strong> <?=Yii::$app->session->getFlash('error',null,true)?>
                                </div>
                            <?php endif;?>
                            <?php if(Yii::$app->session->hasFlash('success')):?>
                                <div class="alert alert-success fade in">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <i class="fa fa-check-circle fa-fw fa-lg"></i>
                                    <strong>Well done!</strong> <?=Yii::$app->session->getFlash('success',null,true)?>
                                </div>
                            <?php endif;?>
                            <div style="display: none" class="alert alert-danger" role="alert">You are not registered. Please click <a href="<?=\yii\helpers\Url::to(['/site/sign-up'])?>">here</a> sign up.</div>
                            <?php $form = ActiveForm::begin(['id' => 'login-form','options'=>['role'=>'form']]); ?>

                            <?= $form->field($model, 'username',['options'=>['class'=>''],'template'=>"<div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-user\"></i></span>{input}</div>{error}"])->textInput(['placeholder'=>'Username']) ?>
                            <?= $form->field($model, 'password',['options'=>['class'=>''],'template'=>"<div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-key\"></i></span>{input}</div>{error}"])->passwordInput(['placeholder'=>'Enter password']) ?>
                            <div id="remember-me-wrapper">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="checkbox-nice">
                                            <input type="checkbox" id="remember-me" checked="checked" />
                                            <label for="remember-me">
                                                Remember me
                                            </label>
                                        </div>
                                    </div>
                                    <a href="<?=\yii\helpers\Url::to(['/site/request-password-reset'])?>" id="login-forget-link" class="col-xs-6">
                                        Forgot password?
                                    </a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <?= Html::submitButton('Sign in', ['class' => 'btn btn-success col-xs-12', 'name' => 'sign-up-button']) ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <p class="social-text">Or login with</p>
                                </div>
                            </div>
                            <div class="row">
                                <?php $authAuthChoice = \yii\authclient\widgets\AuthChoice::begin([
                                    'baseAuthUrl' => ['site/auth'],
                                    'autoRender' => false,
                                ]); ?>

                                <?php foreach ($authAuthChoice->getClients() as $client): ?>
                                    <div class="col-xs-12 col-sm-4">
                                        <a href="<?=$authAuthChoice->createClientUrl($client)?>" class="btn btn-primary col-xs-12 btn-<?=$client->getName()?>">
                                            <i class="fa fa-<?=$client->getName()?>"></i> <?=$client->getName()?>
                                        </a>
                                    </div>
                                <?php endforeach; ?>

                                <?php \yii\authclient\widgets\AuthChoice::end(); ?>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
            </div>

            <div id="login-box-footer">
                <div class="row">
                    <div class="col-xs-12">
                        Do not have an account?
                        <a href="<?=\yii\helpers\Url::to(['/site/sign-up'])?>">
                            Register now
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
