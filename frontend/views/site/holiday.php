<?php
/**@var $model \frontend\models\CalendarForm */
/* @var $this yii\web\View */
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

\frontend\assets\BaseAsset::register($this);
\frontend\assets\DatePickerAsset::register($this);
$script = <<<JS

    $('.holiday').datepicker({
            format: 'dd-mm-yyyy'
    });
JS;
$this->registerJs($script,\yii\web\View::POS_END);
$this->title = 'Holiday | '.Yii::$app->name;
?>
<div class="row">
    <div class="col-lg-4">

    </div>
    <div class="col-lg-4">
        <div class="main-box">
            <header class="main-box-header clearfix">
                <h2>Holiday</h2>
            </header>

            <div class="main-box-body clearfix">
                <?php ?>
                <?php $form = ActiveForm::begin(
                    [
                        'id' => 'holiday-form',
                        'enableAjaxValidation' => true,
                        'validateOnSubmit' => true,
                        'validateOnChange' => false,
                        'validateOnBlur' => false,
                    ]
                );  ?>

                <?= $form->field($model, 'title')
                    ->textInput(['class'=>'form-control','maxlength'=>100])->label('Short Description')
                ?>
                <?= $form->field($model, 'start')
                    ->textInput(['class'=>'form-control holiday'])->label('Start at')
                ?>
                <?= $form->field($model, 'end')
                    ->textInput(['class'=>'form-control holiday'])->label('Ends at')
                ?>
                <?= $form->field($model, 'description')
                    ->textarea(['rows'=>5,'maxlength'=>254])->label('Description')
                ?>
                <div class="row">
                    <div class="col-xs-12">
                        <?= Html::submitButton(Yii::$app->request->get('edit')?'Save':'Add', ['class' => 'btn btn-success col-xs-12', 'name' => 'sign-up-button']) ?>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
    <div class="col-lg-4">

    </div>
</div>