<?php
use frontend\models\User;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $model frontend\models\CalendarForm */
/* @var $dataProvider yii\data\ActiveDataProvider */
\frontend\assets\BaseAsset::register($this);
\frontend\assets\EditableAsset::register($this);
\frontend\assets\IE9Asset::register($this);
$statusList = \yii\helpers\Json::encode(\common\models\Calendar::statusList());
$url = \yii\helpers\Url::to(['site/holidays','update'=>'yes']);
$this->title = 'Holidays | '.Yii::$app->name;
/**@var $user User*/
$user = Yii::$app->user->identity;
?>
<div class="row">
    <div class="col-lg-12">

        <div class="row">
            <div class="col-lg-12">
                <ol class="breadcrumb">
                    <li><a href="<?=\yii\helpers\Url::base()?>">Home</a></li>
                    <li class="active"><span>Holidays</span></li>
                </ol>

                <div class="clearfix">
                    <h1 class="pull-left">Holidays</h1>

                    <?php if ($user->role >= User::ROLE_ADMIN): ?>
                        <div class="pull-right top-page-ui">
                            <a href="<?= \yii\helpers\Url::to(['site/holiday']) ?>" class="btn btn-primary pull-right">
                                <i class="fa fa-plus-circle fa-lg"></i> Add holiday
                            </a>
                        </div>
                    <?php endif ?>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="main-box no-header clearfix">
                    <div class="main-box-body clearfix">

                        <?php
                        Pjax::begin(['enablePushState'=>false]);

                        $columns = [
                            [
                                'attribute'   => 'title',
                                'format'      => 'text',
                                'label'       => 'Shor Desc',
                            ],

                            [
                                'attribute'   => 'description',
                                'format'      => 'text',
                            ],

                            [
                                'attribute'   => 'work_days',
                                'label'      => 'Days',
                                'format'      => 'integer',
                            ],

                            [
                                'attribute'   => 'start',
                                'label'      => 'Starts',
                                'format'      => ['date', 'php:F, d'],
                            ],
                            [
                                'attribute'   => 'end',
                                'label'      => 'Ends',
                                'format'      => ['date', 'php:F, d'],
                            ],

                            [
                                'attribute'     => 'status',
                                'format'        => 'raw',
                                'filter'        => \yii\helpers\ArrayHelper::getColumn(\common\models\Calendar::statusList(),'text'),
                                'value' => function (\common\models\Calendar $cal) {
                                    $class  = 'calendar-status editable editable-click label ';
                                    $label  = '';
                                    $status = \common\models\Calendar::statusList()[$cal->status];
                                    $class .= $status['class'];
                                    $label .= $status['text'];
                                    return "<span class= \"{$class}\" data-type=\"select\" data-name=\"status\" data-title=\"Select status\" data-value=\"{$cal->status}\" data-pk=\"{$cal->id}\">{$label}</span>";
                                },
                                'headerOptions' => ['class'=>'text-center'],
                                'contentOptions'=>['class'=>'text-center'],
                            ],
                        ];
                        if($user->role >= User::ROLE_PM) {
                            $script = <<<JS
    $(document).ready(function(){

        var source1 = [],statusList = $statusList;

        $.each(statusList, function(k, v) {
            source1.push({value: parseInt(k), text: v.text});
        });

        $('.calendar-status').editable({
            url:'$url',
            ajaxOptions: {
                type: 'get',
                dataType: 'json'
            },
            send: 'always',
            success: function(response, newValue) {
               if(response.status!='success') return response.msg;
            },
            error: function(response, newValue) {
                if(response.status === 500) {
                    return 'Service unavailable. Please try later.';
                } else {
                    return response.responseText;
                }
            },
            source: source1,
            mode:'inline',
            highlight:false,
            showbuttons:false,
            select2: {
                width: 200
            },
            display: function(value) {
                $(this).removeClass('label-warning')
                .removeClass('label-danger')
                .removeClass('label-success')
                .addClass(statusList[value].class)
                .text(statusList[value].text);
            }
        });
    });

JS;

                            $this->registerJs($script, \yii\web\View::POS_END);
                            $columns[] = [
                                'class'   => '\yii\grid\ActionColumn',
                                'template'=> '{update} {delete}',
                                'buttons' =>[
                                    'delete'=>function ($url, $model, $key) {
                                        $url = \yii\helpers\Url::to(['site/holidays','delete'=>$model->id]);
                                        $html = '
                                                <a href="'.$url.'" class="table-link danger">
                                                    <span class="fa-stack">
                                                        <i class="fa fa-square fa-stack-2x"></i>
                                                        <i class="fa fa-trash-o fa-stack-1x fa-inverse"></i>
                                                    </span>
                                                </a>';
                                        return $html;
                                    },
                                    'update'=>function ($url, $model, $key) {
                                        $url = \yii\helpers\Url::to(['site/holiday','edit'=>$model->id]);
                                        $html = '
                                               <a data-pjax="0" href="'.$url.'" class="table-link">
                                                    <span class="fa-stack">
                                                        <i class="fa fa-square fa-stack-2x"></i>
                                                        <i class="fa fa-pencil fa-stack-1x fa-inverse"></i>
                                                    </span>
                                               </a>';
                                        return $html;
                                    },
                                ]
                            ];
                        }


                        echo GridView::widget([
                            'dataProvider'  => $dataProvider,
                            'id'            => 'holidays',
                            'tableOptions'  => ['class'=>'table user-list table-hover'],
                            'options'       => ['class' => 'table-responsive'],
                            'pager'         => ['options'=>['class'=>'pagination pull-right']],
                            'filterModel'   => $model,
                            'columns'       => $columns,
                        ]);
                        Pjax::end();
                        ?>

                    </div>
                </div>
            </div>
        </div>

    </div>
</div>