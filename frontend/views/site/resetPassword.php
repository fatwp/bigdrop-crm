<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */


$this->title = 'Reset password | '.Yii::$app->name;
$this->params['breadcrumbs'][] = $this->title;
\frontend\assets\BaseAsset::register($this);
?>

<div class="row">
    <div class="col-xs-12">
        <div id="login-box">
            <div id="login-box-holder">
                <div class="row">
                    <div class="col-xs-12">
                        <header id="login-header">
                            <div id="login-logo">
                                <img src="img/logo.png" alt=""/>
                            </div>
                        </header>
                        <div id="login-box-inner" class="with-heading">
                            <h4>Password recovery</h4>
                            <p>
                                Enter your your new password.
                            </p>
                            <?php $form = ActiveForm::begin(['id' => 'reset-password-form','options'=>['role'=>'form']]); ?>
                            <?= $form->field($model, 'password',['options'=>['class'=>''],'template'=>"<div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-lock\"></i></span>{input}</div>{error}"])->passwordInput(['placeholder'=>'Enter password']) ?>
                            <?= $form->field($model, 'confirmPassword',['options'=>['class'=>''],'template'=>"<div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-unlock-alt\"></i></span>{input}</div>{error}"])->passwordInput(['placeholder'=>'Re-enter password']) ?>
                            <div class="row">
                                <div class="col-xs-12">
                                    <button type="submit" class="btn btn-success col-xs-12">Save</button>
                                </div>
                                <div class="col-xs-12">
                                    <br/>
                                    <a href="<?=\yii\helpers\Url::to(['/site/login'])?>" id="login-forget-link" class="forgot-link col-xs-12">Back to login</a>
                                </div>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
