<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */


$this->title = 'Forgot password | '.Yii::$app->name;
$this->params['breadcrumbs'][] = $this->title;
\frontend\assets\BaseAsset::register($this);
?>

<div class="row">
    <div class="col-xs-12">
        <div id="login-box">
            <?php if(Yii::$app->session->hasFlash('success')):?>
            <div class="alert alert-success">
                <i class="fa fa-check-circle fa-fw fa-lg"></i>
                <strong>Well done!</strong> <?=Yii::$app->session->getFlash('success');?>
            </div>
            <?php endif;?>
            <?php if(Yii::$app->session->hasFlash('error')):?>
            <div class="alert alert-danger">
                <i class="fa fa-times-circle fa-fw fa-lg"></i>
                <strong>Oh snap!</strong> <?=Yii::$app->session->getFlash('error');?>
            </div>
            <?php endif;?>
            <div id="login-box-holder">
                <div class="row">
                    <div class="col-xs-12">
                        <header id="login-header">
                            <div id="login-logo">
                                <img src="img/logo.png" alt=""/>
                            </div>
                        </header>
                        <div id="login-box-inner" class="with-heading">
                            <h4>Forgot your password?</h4>
                            <p>
                                Enter your email address to recover your password.
                            </p>
                            <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form','options'=>['role'=>'form']]); ?>
                                <?= $form->field($model, 'email',['options'=>['class'=>''],'template'=>"<div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-envelope-o\"></i></span>{input}</div>{error}"])->textInput(['placeholder'=>'Email address']) ?>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <button type="submit" class="btn btn-success col-xs-12">Reset password</button>
                                    </div>
                                    <div class="col-xs-12">
                                        <br/>
                                        <a href="<?=\yii\helpers\Url::to(['/site/login'])?>" id="login-forget-link" class="forgot-link col-xs-12">Back to login</a>
                                    </div>
                                </div>
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
