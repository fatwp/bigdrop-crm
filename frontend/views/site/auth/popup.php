<?php
use yii\helpers\Json;

/* @var $this \yii\base\View */
/* @var $url string */
/* @var $attributes array */
?>
<!DOCTYPE html>
<html>
<head>

        <script>

            window.opener.focus();
            window.close();
            var event = new CustomEvent('auth', { 'detail': <?=Json::encode($attributes)?> });
            window.opener.document.dispatchEvent(event);

        </script>

</head>
<body>

</body>
</html>