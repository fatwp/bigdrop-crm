<?php
use frontend\models\User;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $model frontend\models\User */
/* @var $dataProvider yii\data\ActiveDataProvider */
\frontend\assets\BaseAsset::register($this);
\frontend\assets\EditableAsset::register($this);
\frontend\assets\IE9Asset::register($this);
$statusList = \yii\helpers\Json::encode(User::statusList());
$roleList = \yii\helpers\Json::encode(User::roleList());
$url = \yii\helpers\Url::to(['site/users','update'=>'yes']);
$this->title = 'Administrators | '.Yii::$app->name;
?>
<div class="row">
    <div class="col-lg-12">

        <div class="row">
            <div class="col-lg-12">
                <ol class="breadcrumb">
                    <li><a href="<?=\yii\helpers\Url::base()?>">Home</a></li>
                    <li class="active"><span>Administrators</span></li>
                </ol>

                <div class="clearfix">
                    <h1 class="pull-left">Administrators</h1>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="main-box no-header clearfix">
                    <div class="main-box-body clearfix">

                        <?php
                        Pjax::begin();
                        /**@var $user User*/
                        $user = Yii::$app->user->identity;
                        $columns = [
                            [
                                'attribute'   => 'fullName',
                                'format'      => 'Html',
                                'value' => function (User $user) {
                                    $returnVal  = '';
                                    $returnVal .= Html::img($user->profile->getAvatar('small'),['style'=>'border-radius:50%']);
                                    $returnVal .= Html::a($user->fullName?:$user->username,\yii\helpers\Url::to(['user/profile','username'=>$user->username]),['class'=>'user-link']);
                                    $returnVal .= Html::tag('span',$user->getRole(),['class'=>'user-subhead']);
                                    return $returnVal;
                                },
                            ],
                            [
                                'attribute'   => 'email',
                                'format'      => 'email',
                            ],
                            [
                                'attribute'   => 'created_at',
                                'label'      => 'Registered',
                                'format'      => ['date', 'php:m-d-Y'],
                            ],
                            [
                                'attribute'     => 'role',
                                'format'        => 'raw',
                                'filter'        => User::roleList(),
                                'value' => function (User $user) {
                                    $class  = ' text ';
                                    if(Yii::$app->user->id != $user->id){
                                        $class.='user-role editable editable-click';
                                    }
                                    $label  = $user->getRole();
                                    return "<span class= \"{$class}\" data-type=\"select\" data-name=\"role\" data-title=\"Select role\" data-value=\"{$user->role}\" data-pk=\"{$user->id}\">{$label}</span>";
                                },
                                'headerOptions' => ['class'=>'text-center'],
                                'contentOptions'=>['class'=>'text-center'],
                            ],
                            [
                                'attribute'     => 'status',
                                'format'        => 'raw',
                                'filter'        => \yii\helpers\ArrayHelper::getColumn(User::statusList(),'text'),
                                'value' => function (User $user) {
                                    $class  = ' label ';
                                    if(Yii::$app->user->id != $user->id){
                                        $class.='user-status editable editable-click';
                                    }
                                    $label  = '';
                                    $status = User::statusList()[$user->status];
                                    $class .= $status['class'];
                                    $label .= $status['text'];
                                    return "<span class= \"{$class}\" data-type=\"select\" data-name=\"status\" data-title=\"Select status\" data-value=\"{$user->status}\" data-pk=\"{$user->id}\">{$label}</span>";
                                },
                                'headerOptions' => ['class'=>'text-center'],
                                'contentOptions'=>['class'=>'text-center'],
                            ],
                        ];
                        if($user->role >= User::ROLE_ADMIN) {
                            $script = <<<JS
    $(document).ready(function(){

        var source1 = [],source2 = [],statusList = $statusList,roleList = $roleList;

        $.each(statusList, function(k, v) {
            source1.push({value: parseInt(k), text: v.text});
        });

        $.each(roleList, function(k, v) {
            source2.push({value: parseInt(k), text: v});
        });

        $('.user-role').editable({
            url:'$url',
            ajaxOptions: {
                type: 'get',
                dataType: 'json'
            },
            send: 'always',
            success: function(response, newValue) {
               if(response.status!='success') return response.message;
               else $(this).parents('tr').find('span.user-subhead').text(roleList[newValue]);
            },
            source: source2,
            mode:'inline',
            highlight:false,
            showbuttons:false,
            error: function(response, newValue) {
                if(response.status === 500) {
                    return 'Service unavailable. Please try later.';
                } else {
                    return response.responseText;
                }
            },
            select2: {
                width: 200
            }
        });

        $('.user-status').editable({
            url:'$url',
            ajaxOptions: {
                type: 'get',
                dataType: 'json'
            },
            send: 'always',
            success: function(response, newValue) {
               if(response.status!='success') return response.msg;
            },
            error: function(response, newValue) {
                if(response.status === 500) {
                    return 'Service unavailable. Please try later.';
                } else {
                    return response.responseText;
                }
            },
            source: source1,
            mode:'inline',
            highlight:false,
            showbuttons:false,
            select2: {
                width: 200
            },
            display: function(value) {
                $(this).removeClass('label-warning')
                .removeClass('label-danger')
                .removeClass('label-success')
                .removeClass('label-default')
                .addClass(statusList[value].class)
                .text(statusList[value].text);
            }
        });
    });

JS;

                            $this->registerJs($script, \yii\web\View::POS_END);
                            $columns[] = [
                                'class'   => '\yii\grid\ActionColumn',
                                'template'=> '{update} {delete}',
                                'buttons' =>[
                                    'delete'=>function ($url, $model, $key) {
                                        $url = \yii\helpers\Url::to(['site/admins','delete'=>$model->id]);
                                        $html = '
                                                <a href="'.$url.'" class="table-link danger">
                                                    <span class="fa-stack">
                                                        <i class="fa fa-square fa-stack-2x"></i>
                                                        <i class="fa fa-trash-o fa-stack-1x fa-inverse"></i>
                                                    </span>
                                                </a>';
                                        return $model->id == Yii::$app->user->id ? '':$html;
                                    },
                                    'update'=>function ($url, $model, $key) {
                                        $url = \yii\helpers\Url::to(['site/user','edit'=>$model->id]);
                                        $html = '
                                               <a data-pjax="0" href="'.$url.'" class="table-link">
                                                    <span class="fa-stack">
                                                        <i class="fa fa-square fa-stack-2x"></i>
                                                        <i class="fa fa-pencil fa-stack-1x fa-inverse"></i>
                                                    </span>
                                               </a>';
                                        return $model->id == Yii::$app->user->id ? '':$html;
                                    },
                                ]
                            ];
                        }
                        echo GridView::widget([
                            'dataProvider'  => $dataProvider,
                            'id'            => 'users',
                            'tableOptions'  => ['class'=>'table user-list table-hover'],
                            'options'       => ['class' => 'table-responsive'],
                            'pager'         => ['options'=>['class'=>'pagination pull-right']],
                            'filterModel'   => $model,
                            'columns'       => $columns,
                        ]);
                        Pjax::end();
                        ?>

                    </div>
                </div>
            </div>
        </div>

    </div>
</div>