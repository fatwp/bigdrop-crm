<?php
/* @var $this yii\web\View */
use frontend\models\User;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = Yii::$app->name;
\frontend\assets\BaseAsset::register($this);
\frontend\assets\TimelineAsset::register($this);
$this->params['timeline-init'] = 'onload="onLoad();" onresize="onResize();"';

$events = (new \yii\db\Query())->where(['status'=>\common\models\Calendar::STATUS_APPROVED])->select(['title','description','start','end','type'])->from(\console\migrations\Migration::TABLE_CALENDAR)->all();

array_walk(
    $events,
    function(&$item, $key,$holiday) {
        $item['start'] = (new DateTime($item['start']))->format('r');
        if($item['end'])
            $item['end'] = (new DateTime($item['end']))->format('r');
        else
            unset($item['end']);
        if($holiday == $item['type']){
            $item['color'] = 'red';
            $item['textColor'] = 'green';
            $item['caption'] = 'Holiday';
        }

    },
    \common\models\Calendar::TYPE_HOLIDAY
);
$calendar['events'] = $events;
$calendar = \yii\helpers\Json::encode($calendar);
$script =<<<JS

        var tl;
        function onLoad() {
            var eventSource = new Timeline.DefaultEventSource(0);
            var now = new Date();
            var timestamp = now.getTime();



            var theme = Timeline.ClassicTheme.create();

            theme.event.bubble.width = 250;

            var bandInfos = [
                Timeline.createBandInfo({
                    width:          "80%",
                    intervalUnit:   Timeline.DateTime.DAY,
                    intervalPixels: 50,
                    eventSource:    eventSource,
                    date:           now,

                }),
                Timeline.createBandInfo({
                    width:          "20%",
                    intervalUnit:   Timeline.DateTime.MONTH,
                    intervalPixels: 200,
                    eventSource:    eventSource,
                    date:           now,
                    overview:       true

                })
            ];
            bandInfos[1].syncWith = 0;
            bandInfos[1].highlight = true;


            tl = Timeline.create(document.getElementById("tl"), bandInfos, Timeline.HORIZONTAL);
            // stop browser caching of data during testing...
            tl.loadJSON("cubism.js?"+ (new Date().getTime()), function(json, url) {

                eventSource.loadJSON($calendar,url);
            });
            //var _timeline = document.getElementById('tl');
            //_timeline.className = _timeline.className += ' default-theme';
        }

        var resizeTimerID = null;
        function onResize() {
            if (resizeTimerID == null) {
                resizeTimerID = window.setTimeout(function() {
                    resizeTimerID = null;
                    tl.layout();
                }, 500);
            }
        }

        /*function themeSwitch(){
			var timeline = document.getElementById('tl');
			timeline.className = (timeline.className.indexOf('dark-theme') != -1) ? timeline.className.replace('dark-theme', 'default-theme') : timeline.className.replace('default-theme', 'dark-theme');

		}*/

		function themeSwitch(){
			var timeline = document.getElementById('tl');
			timeline.className = (timeline.className.indexOf('dark-theme') != -1) ? timeline.className.replace('dark-theme', '') : timeline.className += ' dark-theme';

		}

		Timeline.GregorianDateLabeller.prototype.labelPrecise = function(date) {
            return SimileAjax.DateTime.removeTimeZoneOffset(
                date,
                this._timeZone + (new Date().getTimezoneOffset() / 60)
            ).toLocaleDateString();
        };


JS;
$this->registerJs($script,\yii\web\View::POS_HEAD);

?>
<div class="row" >
    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-12">
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li class="active"><span>Dashboard</span></li>
                </ol>

                <h1>Dashboard</h1>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-3 col-sm-6 col-xs-12">
        <div class="main-box infographic-box">
            <i class="fa fa-user red-bg"></i>
            <span class="headline">PM</span>
										<span class="value">
											<span class="timer" ><?=\frontend\models\User::find()->where(['role'=>\frontend\models\User::ROLE_PM])->count()?></span>
										</span>
        </div>
    </div>
    <div class="col-lg-3 col-sm-6 col-xs-12">
        <div class="main-box infographic-box">
            <i class="fa fa-user emerald-bg"></i>
            <span class="headline">Designer</span>
										<span class="value">
											<span class="timer" ><?=\frontend\models\User::find()->where(['role'=>\frontend\models\User::ROLE_DESIGNER])->count()?></span>
										</span>
        </div>
    </div>
    <div class="col-lg-3 col-sm-6 col-xs-12">
        <div class="main-box infographic-box">
            <i class="fa fa-user green-bg"></i>
            <span class="headline">Developer</span>
										<span class="value">
											<span class="timer" ><?=\frontend\models\User::find()->where(['role'=>\frontend\models\User::ROLE_DEVELOPER])->count()?></span>
										</span>
        </div>
    </div>
    <div class="col-lg-3 col-sm-6 col-xs-12">
        <div class="main-box infographic-box">
            <i class="fa fa-user yellow-bg"></i>
            <span class="headline">QA</span>
										<span class="value">
											<span class="timer" ><?=\frontend\models\User::find()->where(['role'=>\frontend\models\User::ROLE_QA])->count()?></span>
										</span>
        </div>
    </div>
</div>

<?php
/* @var $provider yii\data\ActiveDataProvider */

$provider = new ActiveDataProvider([
    'query' => \frontend\models\User::find()->andWhere(['<>','role',User::ROLE_ADMIN])->joinWith('profile')->select(['user.*',"CONCAT_WS(' ',first_name,last_name) AS name"]),
    'pagination' => [
        'pageSize' => 5,
    ],
    'sort'=>[
        'defaultOrder'=>[
            'created_at' => SORT_DESC,
        ],
        'attributes' => [
            'created_at',
        ],
    ]
]);

/**@var $user User*/
$user = Yii::$app->user->identity;

if(in_array($user->role,[User::ROLE_ADMIN,User::ROLE_PM])) {
    $statusList = \yii\helpers\Json::encode(User::statusList());
    $roleList = \yii\helpers\Json::encode(User::roleList());
    $url = \yii\helpers\Url::to(['site/users','update'=>'yes']);
    \frontend\assets\EditableAsset::register($this);
    $script = <<<JS
    $(document).ready(function(){

        var source1 = [],source2 = [],statusList = $statusList,roleList = $roleList;

        $.each(statusList, function(k, v) {
            source1.push({value: parseInt(k), text: v.text});
        });

        $.each(roleList, function(k, v) {
            source2.push({value: parseInt(k), text: v});
        });

        $('.user-role').editable({
            url:'$url',
            ajaxOptions: {
                type: 'get',
                dataType: 'json'
            },
            send: 'always',
            success: function(response, newValue) {
               if(response.status!='success') return response.message;
               else $(this).parents('tr').find('span.user-subhead').text(roleList[newValue]);
            },
            source: source2,
            mode:'inline',
            highlight:false,
            showbuttons:false,
            error: function(response, newValue) {
                if(response.status === 500) {
                    return 'Service unavailable. Please try later.';
                } else {
                    return response.responseText;
                }
            },
            select2: {
                width: 200
            }
        });

        $('.user-status').editable({
            url:'$url',
            ajaxOptions: {
                type: 'get',
                dataType: 'json'
            },
            send: 'always',
            success: function(response, newValue) {
               if(response.status!='success') return response.msg;
            },
            error: function(response, newValue) {
                if(response.status === 500) {
                    return 'Service unavailable. Please try later.';
                } else {
                    return response.responseText;
                }
            },
            source: source1,
            mode:'inline',
            highlight:false,
            showbuttons:false,
            select2: {
                width: 200
            },
            display: function(value) {
                $(this).removeClass('label-warning')
                .removeClass('label-danger')
                .removeClass('label-success')
                .removeClass('label-default')
                .addClass(statusList[value].class)
                .text(statusList[value].text);
            }
        });
    });

JS;
    $this->registerJs($script,\yii\web\View::POS_END);
}

?>
<div class="row">
    <div class="col-lg-12">
        <div class="main-box no-header clearfix">
            <div class="main-box-body clearfix">

                <?php

                echo GridView::widget([
                    'dataProvider'  => $provider,
                    'id'            => 'users',
                    'tableOptions'  => ['class'=>'table user-list table-hover'],
                    'options'       => ['class' => 'table-responsive'],
                    'layout'        => '{items}',
                    'columns'       => [

                        [
                            'attribute'   => 'fullName',
                            'format'      => 'Html',
                            'value' => function (User $user) {
                                $returnVal  = '';
                                $returnVal .= Html::img($user->profile->getAvatar('small'),['style'=>'border-radius:50%']);
                                $returnVal .= Html::a($user->getFullName()?:$user->username,\yii\helpers\Url::to(['user/profile','username'=>$user->username]),['class'=>'user-link']);
                                $returnVal .= Html::tag('span',$user->getRole(),['class'=>'user-subhead']);
                                return $returnVal;
                            },
                        ],
                        [
                            'attribute'   => 'email',
                            'format'      => 'email',
                        ],
                        [
                            'attribute'   => 'created_at',
                            'label'      => 'Registered',
                            'format'      => ['date', 'php:m-d-Y'],
                        ],
                        [
                            'attribute'     => 'role',
                            'format'        => 'raw',
                            'filter'        => User::roleList(),
                            'value' => function (User $user) {
                                $class  = 'user-role editable editable-click text';
                                $label  = $user->getRole();
                                return "<span class= \"{$class}\" data-type=\"select\" data-name=\"role\" data-title=\"Select role\" data-value=\"{$user->role}\" data-pk=\"{$user->id}\">{$label}</span>";
                            },
                            'headerOptions' => ['class'=>'text-center'],
                            'contentOptions'=>['class'=>'text-center'],
                        ],
                        [
                            'attribute'     => 'status',
                            'format'        => 'raw',
                            'filter'        => \yii\helpers\ArrayHelper::getColumn(User::statusList(),'text'),
                            'value' => function (User $user) {
                                $class  = 'user-status editable editable-click label ';
                                $label  = '';
                                $status = User::statusList()[$user->status];
                                $class .= $status['class'];
                                $label .= $status['text'];
                                return "<span class= \"{$class}\" data-type=\"select\" data-name=\"status\" data-title=\"Select status\" data-value=\"{$user->status}\" data-pk=\"{$user->id}\">{$label}</span>";
                            },
                            'headerOptions' => ['class'=>'text-center'],
                            'contentOptions'=>['class'=>'text-center'],
                        ],

                    ],
                ]);
                ?>

            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-lg-12">
        <div class="pull-left">
            <div class="onoffswitch onoffswitch">
                <input name="onoffswitch3" class="onoffswitch-checkbox" id="myonoffswitch3" type="checkbox">
                <label class="onoffswitch-label" for="myonoffswitch3">
                    <div onclick="themeSwitch();" class="onoffswitch-inner"></div>
                    <div onclick="themeSwitch();" class="onoffswitch-switch"></div>
                </label>
            </div>
        </div>
        <span class="help-block">Dark theme</span>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-12">
                <div id="tl" class="timeline-default" style="height: 300px;">
                </div>
            </div>
        </div>
    </div>
</div>




