<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\UserSignUpForm */

$this->title = 'Sign up | '.Yii::$app->name;
$this->params['breadcrumbs'][] = $this->title;

$script=<<<INLINE
    document.addEventListener('auth', function (e) {
        console.log(e);
        $.each(e.detail, function(k, v) {
            if(k!='gender'){
                $('#form-sign-up [name*='+k+']').val(v);
            }else{
                $('#form-sign-up [name*='+k+']').attr('checked','').parent().removeClass('active');
                $('#form-sign-up').find('[value='+v+']').attr('checked','checked').parent().addClass('active');
            }
        });
    }, false);
INLINE;

$this->registerJs($script);

\frontend\assets\BaseAsset::register($this);

?>
<div class="row">
    <div class="col-xs-12">
        <div id="login-box">
            <div class="login-box-holder">
                <div class="row">
                <div class="col-xs-12">
                    <header id="login-header">
                        <div id="login-logo">
                            <img src="/img/logo.png" alt=""/>
                        </div>
                    </header>
                    <div id="login-box-inner">
                        <?php if(Yii::$app->session->hasFlash('error')):?>
                            <div class="alert alert-danger fade in">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <i class="fa fa-times-circle fa-fw fa-lg"></i>
                                <strong>Oh snap!</strong> <?=Yii::$app->session->getFlash('error',null,true)?>
                            </div>
                        <?php endif;?>
                        <?php if(Yii::$app->session->hasFlash('success')):?>
                            <div class="alert alert-success fade in">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <i class="fa fa-check-circle fa-fw fa-lg"></i>
                                <strong>Well done!</strong> <?=Yii::$app->session->getFlash('success',null,true)?>
                            </div>
                        <?php endif;?>

                        <?php $form = ActiveForm::begin(['id' => 'form-sign-up','options'=>['role'=>'form'],'enableAjaxValidation'=>false,'enableClientValidation'=>false]); ?>

                        <?= $form->field($model, 'username',['options'=>['class'=>''],'template'=>"<div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-user\"></i></span>{input}</div>{error}"])->textInput(['placeholder'=>'Username']) ?>
                        <?= $form->field($model, 'email',['options'=>['class'=>''],'template'=>"<div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-envelope-o\"></i></span>{input}</div>{error}"])->textInput(['placeholder'=>'Email address']) ?>
                        <?= $form->field($model, 'password',['options'=>['class'=>''],'template'=>"<div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-lock\"></i></span>{input}</div>{error}"])->passwordInput(['placeholder'=>'Enter password']) ?>
                        <?= $form->field($model, 'confirmPassword',['options'=>['class'=>''],'template'=>"<div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-unlock\"></i></span>{input}</div>{error}"])->passwordInput(['placeholder'=>'Re-enter password']) ?>
                        <?= $form->field($model, 'firstName',['options'=>['class'=>''],'template'=>"<div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa\">F</i></span>{input}</div>{error}"])->textInput(['placeholder'=>'First name']) ?>
                        <?= $form->field($model, 'lastName',['options'=>['class'=>''],'template'=>"<div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa\">L</i></span>{input}</div>{error}"])->textInput(['placeholder'=>'Last name']) ?>
                        <?= $form->field($model, 'position',['options'=>['class'=>''],'template'=>"<div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa\">P</i></span>{input}</div>{error}"])->textInput(['placeholder'=>'Position']) ?>
                        <div class="row">

                        <?= $form->field($model, 'gender',['options'=>['class'=>'col-xs-12'],'wrapperOptions'=>['class'=>'btn-group col-xs-12','data-toggle'=>'buttons']])->radioList(
                            [
                                \common\models\Profile::GENDER_MALE=>'Male',
                                \common\models\Profile::GENDER_FEMALE=>'Female'
                            ],
                            [
                                'item'=>function ($index, $label, $name, $checked, $value) {
                                    return '<label class="btn btn-'.($value==\common\models\Profile::GENDER_MALE?'danger':'warning').' col-xs-6 '.($checked?'active':'').'">' . Html::input('radio',$name, $value, ['checked' => $checked]) . '<i class="fa fa-'.($value==\common\models\Profile::GENDER_MALE?'male':'female').'"></i> '.$label.'</label>';
                                },
                                'class'=>'btn-group col-xs-12','data-toggle'=>'buttons',
                            ]
                        )
                            ->label(false) ?>

                        </div>
                        <?= $form->field($model, 'sourceId',['options'=>['class'=>'']])->hiddenInput()->label(false) ?>
                        <?= $form->field($model, 'sourceName',['options'=>['class'=>'']])->hiddenInput()->label(false) ?>
                        <div class="row">
                            <div class="col-xs-12">
                                <?= Html::submitButton('Sign up', ['class' => 'btn btn-success col-xs-12', 'name' => 'sign-up-button']) ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <p class="social-text">Or sign up with</p>
                            </div>
                        </div>
                        <div class="row">
                            <?php $authAuthChoice = \yii\authclient\widgets\AuthChoice::begin([
                                'baseAuthUrl' => ['site/auth'],
                                'autoRender' => false,
                            ]); ?>

                            <?php foreach ($authAuthChoice->getClients() as $client): ?>
                                <div class="col-xs-12 col-sm-4">
                                    <a href="<?=$authAuthChoice->createClientUrl($client)?>" class="btn btn-primary col-xs-12 btn-<?=$client->getName()?>">
                                        <i class="fa fa-<?=$client->getName()?>"></i> <?=$client->getName()?>
                                    </a>
                                </div>
                            <?php endforeach; ?>

                            <?php \yii\authclient\widgets\AuthChoice::end(); ?>

                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
            </div>
            <div id="login-box-footer">
                <div class="row">
                    <div class="col-xs-12">
                        Do you have an account?
                        <a href="<?=\yii\helpers\Url::to(['/site/login'])?>">
                            Sign in
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

