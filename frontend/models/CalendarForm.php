<?php

namespace frontend\models;

use common\models\ActiveRecord;
use common\models\Calendar;
use Yii;
use yii\base\InvalidParamException;
use yii\base\Model;
use yii\db\Exception;
use yii\helpers\VarDumper;

/**
 * CalendarForm is the model behind the contact form.
 */
class CalendarForm extends Model
{
    public $username;
    public $title;
    public $description;
    public $start;
    public $end;
    public $id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            ['username', 'match','pattern'=>UserForm::USERNAME_PATTERN,'message'=>'User not found',],
            ['username', 'string', 'min' => 2, 'max' => 10,'message'=>'User not found'],
            ['username', 'exist','targetClass'=>'\frontend\models\User','message'=>'User not found'],

            ['description', 'filter', 'filter' =>function($value){return trim(strip_tags($value));}],
            ['description', 'required'],
            ['description','string','length'=>[10,254]],

            ['title', 'filter', 'filter' =>function($value){return trim(strip_tags($value));}],
            ['title', 'required'],
            ['title', 'string','length'=>[5,100]],

            [['start','end'],'required'],
            [['start','end'],'date','format'=>'php:d-m-Y'],
            [['start','end'],'compareDate','params'=>['operator'=>'>','compareValue'=>'now'],'on'=>'vacation'],
            ['start','compareDate','params'=>['operator'=>'<=','compareAttribute'=>'end']],
            ['end','compareDate','params'=>['operator'=>'>=','compareAttribute'=>'start']],
            [['start','end'],'notAHoliday','on'=>'holiday'],
            [['end'],'notIncludeHolidays','on'=>'holiday'],

            ['id','integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'holiday'=> ['title','description','start','end'],
            'vacation'=> ['username','title','description','start','end'],
        ];
    }

    public function notIncludeHolidays($attribute,$params)
    {
        if($this->start && $this->end){
            $start = (new \DateTime($this->start))->format(ActiveRecord::DATE_FORMAT);
            $end   = (new \DateTime($this->end))->format(ActiveRecord::DATE_FORMAT);
            $exists = Calendar::find()
                ->where(['type'=>Calendar::TYPE_HOLIDAY])
                ->andWhere(['and',['>','start',$start],['<','end',$end],['!=','id',$this->id]])
                ->from(\console\migrations\Migration::TABLE_CALENDAR)->exists();

            if($exists){
                $this->addError('end','there are some holidays in this date range');
            }
        }
    }

    public function notAHoliday($attribute,$params)
    {
        $date = (new \DateTime($this->$attribute))->format(ActiveRecord::DATE_FORMAT);
        $exists = Calendar::find()
            ->where(['type'=>Calendar::TYPE_HOLIDAY])
            ->andWhere(['and',['<=','start',$date],['>=','end',$date],['!=','id',$this->id]])
            ->from(\console\migrations\Migration::TABLE_CALENDAR)->exists();
        if($exists){
            $this->addError($attribute,$this->$attribute.' is already a holiday');
        }
    }


    public function compareDate($attribute,$params){
        if(!isset($params['compareValue']) && !isset($params['compareAttribute'])){
            throw new InvalidParamException('You must set compareValue or compareAttribute param');
        }

        if(!isset($params['operator'])){
            throw new InvalidParamException('You must set operator param');
        }

        $compareValue = isset($params['compareValue']) ? $params['compareValue']:$this->{$params['compareAttribute']};
        $condition = false;
        $errorMessage = '';

        switch($params['operator']){
            case '>':
                $condition = date_create($this->$attribute) > date_create($compareValue);
                $errorMessage = 'larger than';
                break;
            case '<':
                $condition = date_create($this->$attribute) < date_create($compareValue);
                $errorMessage = 'smaller than';
                break;
            case '>=':
                $condition = date_create($this->$attribute) >= date_create($compareValue);
                $errorMessage = 'larger or  equal to';
                break;
            case '<=':
                $condition = date_create($this->$attribute) <= date_create($compareValue);
                $errorMessage = 'smaller or  equal to';
                break;
            case '==':
                $condition = date_create($this->$attribute) == date_create($compareValue);
                $errorMessage = 'equal to';
                break;
            default:
                throw new InvalidParamException('Invalid operator');
                break;
        }

        if(!$condition){
            if(isset($params['compareValue'])){
                $this->addError($attribute,$this->getAttributeLabel($attribute).' must be '.$errorMessage.' '.$params['compareValue']);
            }else if(!empty($this->{$params['compareAttribute']})){
                $this->addError($attribute,$this->getAttributeLabel($attribute).' must be '.$errorMessage.' '.$params['compareAttribute'].' field value');
            }
        }

    }

    public function save()
    {
        $tr = Calendar::getDb()->beginTransaction();

        try {
            /**@var $user User */
            /**@var $calendar Calendar */
            if (!$this->username) {
                $user = Yii::$app->user->identity;
            } else {
                $user = User::findByUsername($this->username);
            }

            if ($this->id) {
                $calendar = Calendar::findOne(['id' => $this->id]);
            } else {
                $calendar = new Calendar();
            }

            $calendar->user_id = $user->id;
            $calendar->title = $this->title;
            $calendar->description = $this->description;
            $start = (new \DateTime($this->start))->format(ActiveRecord::DATE_FORMAT);
            $end = (new \DateTime($this->end))->format(ActiveRecord::DATE_FORMAT);
            $calendar->type = strcmp($this->scenario, 'vacation') ? Calendar::TYPE_HOLIDAY : Calendar::TYPE_VACATION;

            if (!$calendar->isNewRecord) {

                if (strcmp($start, $calendar->start)) {
                    if (!strcmp($this->scenario, 'holiday')) {
                        Calendar::updateAll(['work_days' => null],
                            ['and',
                                ['type' => Calendar::TYPE_VACATION],
                                ['or',
                                    ['and', ['<=', 'start', $calendar->start], ['>=', 'end', $calendar->start]],
                                    ['and', ['<=', 'start', $start], ['>=', 'end', $start]]
                                ]
                            ]);

                    }
                    $calendar->work_days = null;
                }

                if (strcmp($end, $calendar->end)) {
                    if (!strcmp($this->scenario, 'holiday')) {
                        Calendar::updateAll(['work_days' => null],
                            ['and',
                                ['type' => Calendar::TYPE_VACATION],
                                ['or',
                                    ['and', ['<=', 'start', $calendar->end], ['>=', 'end', $calendar->end]],
                                    ['and', ['<=', 'start', $end], ['>=', 'end', $end]]
                                ]
                            ]);

                    }
                    $calendar->work_days = null;
                }
            }

            $calendar->start = $start;
            $calendar->end = $end;

            if ($calendar->save()){
                $tr->commit();
                return true;
            }

        } catch (Exception $e) {
            $tr->rollBack();
        }

        return false;

    }

}
