<?php
/**
 * Created by PhpStorm.
 * User: buba
 * Date: 7/22/15
 * Time: 12:36 PM
 */

namespace frontend\models;



use common\models\Auth;
use common\models\Profile;
use yii\base\Exception;
use yii\helpers\ArrayHelper;

class UserSettingForm extends UserForm
{
    public $role;
    public $status;
    public $employment_date;
    public $vacation_days = 0;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge( parent::rules(),[
            ['status', 'in', 'range' => [User::STATUS_ACTIVE, User::STATUS_DELETED, User::STATUS_BANED,User::STATUS_PENDING]],
            ['role', 'in', 'range' => [User::ROLE_ADMIN, User::ROLE_PM, User::ROLE_DEVELOPER,User::ROLE_QA,User::ROLE_DESIGNER,User::ROLE_USER]],

            ['vacation_days','integer'],

            ['vacation_days','compare','operator'=>'<','compareValue'=>180],

            ['employment_date','date','format'=>'php:d-m-Y'],

            ['username', 'uniqueCustom',
                'params'=>['targetClass' => '\frontend\models\User',
                    'message' => 'This username has already been taken.','not'=>['id']]],

            ['email', 'uniqueCustom',
                'params'=>['targetClass' => '\frontend\models\User',
                    'message' => 'This email address has already been taken.','not'=>['id']]],

            ['confirmPassword', 'required',
                'when'=>function($model){
                    return !empty($model->password);
                },
                'whenClient'=>"function (attribute, value) { return $('#usersettingform-password').val() != ''; }"
            ],

        ] );
    }

    public function uniqueCustom($attribute,$params)
    {
        $targetClass = $params['targetClass'];

        $targetAttribute = !isset($params['targetAttribute']) ? $attribute : $this->$params['targetAttribute'];


        if (is_array($targetAttribute)) {
            $where = [];
            foreach ($targetAttribute as $k => $v) {
                $where[$v] = is_int($k) ? $this->$v : $this->$k;
            }
        } else {
            $where = [$targetAttribute => $this->$attribute];
        }

        foreach ($where as $value) {
            if (is_array($value)) {
                $this->addError($attribute, \Yii::t('yii', '{attribute} is invalid.',
                    ['attribute'=>$this->getAttributeLabel($attribute)]));
                return;
            }
        }


        /**@var $query \yii\db\Query */
        $query = $targetClass::find();

        $query->andWhere($where);

        if(isset($params['not'])){
            foreach ($params['not'] as $k => $v) {
                $query->andWhere(['<>',$v,is_int($k) ? $this->$v : $this->$k]);
            }
        }

        if ($query->exists()) {
            $this->addError($attribute, !isset($params['message']) ?
                \Yii::t('yii', '{attribute} must be unique',
                    ['attribute'=>$this->getAttributeLabel($attribute)]):$params['message']);
        }

    }

    public function save(){

        $tr = \Yii::$app->db->beginTransaction();
        try {
            if($this->id){
                /**@var $user User*/
                $user = User::findIdentity($this->id);

                if (strcmp($this->email,$user->email)) {
                    $user->generateEmailConfirmationToken();
                }

            }else{
                $user = new User();
                $user->generateAuthKey();
            }

            $user->username = $this->username;
            $user->email = $this->email;
            $user->role = $this->role;
            $user->status = $this->status;
            if ($this->password) {
                $user->setPassword($this->password);
            }

            if ($user->save()) {
                if ($this->id) {
                    $profile = $user->profile;
                }else{
                    $profile = new Profile();
                }

                $profile->user_id    = $user->id;
                $profile->position = $this->position;
                $profile->first_name = $this->firstName?:null;
                $profile->last_name = $this->lastName?:null;
                $profile->gender = $this->gender?:null;
                $profile->employment_date = $this->employment_date?:null;

                if ($profile->save()) {

                    $tr->commit();

                    if ($this->id) {
                        $user->sendEmailConfirmation();
                    }

                    return $user;

                } else {
                    throw   new Exception(json_encode($profile->getErrors()));
                }
            } else {
                throw   new Exception(json_encode($user->getErrors()));
            }
        } catch (\Exception $ex){
            $tr->rollBack();
            \Yii::error($ex->getMessage(),__METHOD__);
        }
        return null;
    }

}