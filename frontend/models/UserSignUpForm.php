<?php
/**
 * Created by PhpStorm.
 * User: buba
 * Date: 16.04.15
 * Time: 19:29
 */

namespace frontend\models;


use common\models\Profile;
use common\models\Auth;
use yii\base\Exception;
use yii\helpers\Json;


class UserSignUpForm extends BaseSignUpForm{


    public function signUp()
    {
        if ($this->validate()) {

            $tr = \Yii::$app->db->beginTransaction();
            try {
                $user = new User();
                $user->username = $this->username;
                $user->email = $this->email;
                $user->setPassword($this->password);
                $user->generateAuthKey();
                if ($user->save()) {
                    $profile = new Profile();
                    $profile->user_id    = $user->id;
                    $profile->position = $this->position;
                    $profile->first_name = $this->firstName?:null;
                    $profile->last_name = $this->lastName?:null;
                    $profile->gender = $this->gender?:null;
                    if ($profile->save()) {
                        if($this->sourceId && $this->sourceName){
                            $auth = new Auth([
                                'user_id' => $user->id,
                                'source' => $this->sourceName,
                                'source_id' => $this->sourceId,
                            ]);
                            if (!$auth->save()) {
                                throw   new Exception(json_encode($auth->getErrors()));
                            }
                        }

                        $tr->commit();
                        $user->sendEmailConfirmation();
                        return $user;

                    } else {
                        throw   new Exception(json_encode($profile->getErrors()));
                    }
                } else {
                    throw   new Exception(json_encode($user->getErrors()));
                }
            } catch (\Exception $ex){
                $tr->rollBack();
                \Yii::error(Json::encode($ex),__METHOD__);
            }
        }

        return null;
    }

}