<?php
namespace frontend\models;


use yii\base\InvalidParamException;
use yii\base\Model;
use Yii;

/**
 * Password reset form
 */
class ResetPasswordForm extends Model
{
    public $oldPassword;
    public $password;
    public $confirmPassword;
    /**
     * @var \common\models\User
     */
    private $_user;


    /**
     * Creates a form model given a token.
     *
     * @param  string                          $token
     * @param  array                           $config name-value pairs that will be used to initialize the object properties
     * @throws \yii\base\InvalidParamException if token is empty or not valid
     */
    public function __construct($token, $config = [])
    {
        if (Yii::$app->user->isGuest && (empty($token) || !is_string($token))) {
            throw new InvalidParamException('Password reset token cannot be blank.');
        }
        $this->_user = Yii::$app->user->isGuest?User::findByPasswordResetToken($token):Yii::$app->user->identity;

        if (!$this->_user) {
            throw new InvalidParamException('Wrong password reset token.');
        }
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['password', 'required'],
            ['password','compare','compareValue'=>$this->_user->username,'operator'=>'!=','message'=>'You are using your username as password'],
            ['password','compare','compareValue'=>$this->_user->email,'operator'=>'!=','message'=>'You are using your email as password'],
            ['password', 'match','pattern'=>'/^(.){8,20}$/'],
            ['password', 'string', 'min' => 8],

            ['confirmPassword','compare','compareAttribute'=>'password','message'=>'You must exactly repeat the password picked  above'],

            ['oldPassword', 'required','on'=>'reset'],
            ['oldPassword','oldPasswordValidator','on'=>'reset'],
        ];
    }

    /**
     * Resets password.
     *
     * @return boolean if password was reset.
     */
    public function resetPassword()
    {
        $user = $this->_user;
        $user->setPassword($this->password);
        $user->removePasswordResetToken();

        return $user->save();
    }

    public function oldPasswordValidator($attribute,$params){

        if(!Yii::$app->security->validatePassword($this->$attribute,$this->_user->password_hash)){
            $this->addError($attribute,'Incorrect old password');
        }
    }
}
