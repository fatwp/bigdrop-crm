<?php
/**
 * Created by PhpStorm.
 * User: buba
 * Date: 09.04.15
 * Time: 19:28
 */

namespace frontend\models;
use common\models\Profile;
use yii\web\ServerErrorHttpException;


/**
 * @property mixed fullName
 */
class User extends \common\models\User{

    public $fullName;

    public function rules()
    {
        return array_merge(parent::rules(),
            [
                [['fullName','status','role'],'safe','on'=>'search']
            ]
        );

    }


    public function getFullName()
    {
        return $this->profile->fullName;
    }


    public function afterSave($insert, $changedAttributes)
    {
        if($insert){
            //welcome message to user
        }
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * This method is called when the AR object is created and populated with the query result.
     * The default implementation will trigger an [[EVENT_AFTER_FIND]] event.
     * When overriding this method, make sure you call the parent implementation to ensure the
     * event is triggered.
     */
    public function afterFind()
    {
        if($this->profile == null ){
            $profile = new Profile();
            $profile->user_id    = $this->id;
            if(!$profile->save()){
                throw new ServerErrorHttpException('Cannot create profile for user #ID '.$this->id);
            }
        }
    }

    /**
     * @param string $str
     * @return string|null
     */
    public static function parseGender($str){
        if(is_string($str) && !empty($str)){
            return strtoupper(substr($str,0,1));
        }else{
            return null;
        }
    }


}