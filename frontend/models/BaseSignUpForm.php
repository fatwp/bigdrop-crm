<?php
namespace frontend\models;


use yii\helpers\ArrayHelper;
use Yii;

/**
 * Sig nup form
 */
abstract class BaseSignUpForm extends UserForm
{

    //auth fields
    public $sourceId;
    public $sourceName;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge( parent::rules(),[
            
            ['sourceId', 'match','pattern'=>'/^[a-z0-9A-Z_]+$/'],
            ['sourceName', 'in','range'=>['google','facebook','twitter']],
            [['sourceName', 'sourceId'], 'string', 'max' => 255],

            ['password', 'required'],

            ['confirmPassword', 'required'],

            ['username', 'unique',
                'targetClass' => '\frontend\models\User',
                'message' => 'This username has already been taken.',
            ],

            ['email', 'unique',
                'targetClass' => '\frontend\models\User',
                'message' => 'This email address has already been taken.',
            ],

        ] );
    }



    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    abstract  public function signUp();

}
