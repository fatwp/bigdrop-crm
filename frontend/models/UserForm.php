<?php
/**
 * Created by PhpStorm.
 * User: buba
 * Date: 7/22/15
 * Time: 12:16 PM
 */

namespace frontend\models;
use common\models\Profile;
use yii\db\Query;

abstract class UserForm extends \yii\base\Model
{
    const USERNAME_PATTERN = '/^\s*[a-z][0-9a-z_]+\s*$/';

    public $id;
    public $username;
    public $email;
    public $password;
    public $confirmPassword;

    //optional fields
    public $firstName ;
    public $lastName ;
    public $gender ;

    public $position;



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            ['id', 'integer'],

            ['username', 'filter', 'filter' =>function($value){return strtolower(trim(strip_tags($value)));}],
            ['username', 'required'],
            ['username', 'match','pattern'=>self::USERNAME_PATTERN,'message'=>'Username should start with a letter and can contain only (a-z,0-9,_)',],
            ['username', 'string', 'min' => 2, 'max' => 10],

            [['firstName','lastName'], 'required'],
            [['firstName','lastName'], 'filter', 'filter' =>function($value){return ucfirst(trim(strip_tags($value)));}],
            [['firstName','lastName'], 'string', 'min' => 2, 'max' => 30],

            ['gender', 'required'],
            ['gender', 'filter', 'filter' =>function($value){return strtoupper(trim(strip_tags($value)));}],
            ['gender','in','range'=>[Profile::GENDER_FEMALE,Profile::GENDER_MALE]],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],

            ['password','compare','compareAttribute'=>'username','operator'=>'!=','message'=>'You are using your username as password'],
            ['password','compare','compareAttribute'=>'email','operator'=>'!=','message'=>'You are using your email as password'],
            ['password', 'match','pattern'=>'/^(.){8,20}$/'],
            ['password', 'string', 'min' => 8],

            ['confirmPassword','compare','compareAttribute'=>'password','message'=>'You must exactly repeat the password picked  above'],

            ['position','filter','filter'=>function($value){return trim(strip_tags($value));}],
            ['position','required'],
            ['position','string','length'=>[2,30]]
        ];
    }


}