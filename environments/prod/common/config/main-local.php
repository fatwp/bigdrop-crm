<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=166.78.164.43;dbname=crm',
            'username' => 'crm',
            'password' => 'UnNVTrzbsDeCsJhv',
            'charset' => 'utf8',
        ],
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [

                'facebook' => [
                    'class' => 'yii\authclient\clients\Facebook',
                    'clientId' => '491278807692437',
                    'clientSecret' => '7efd94fd697bbadecb680b2fb8a58473',
                ],
                'google' => [
                    'class' => 'yii\authclient\clients\GoogleOAuth',
                    'clientId' => '624157740178-cgq4h4a29rdqvp4f5rm6ck559p4qb2ir.apps.googleusercontent.com',
                    'clientSecret' => 'U8b8wmpiONohgpv5sG6_9KlM',
                ],
                'twitter' => [
                    'class' => 'yii\authclient\clients\Twitter',
                    'consumerKey' => 'twitter_consumer_key',
                    'consumerSecret' => 'twitter_consumer_secret',
                ],

            ],
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
        ],
    ],
];
