<?php

namespace common\models;
use console\migrations\Migration;
use Yii;
use yii\web\ForbiddenHttpException;

/**
 * This is the model class for table "calendar".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $type
 * @property string $title
 * @property string $description
 * @property string $start
 * @property string $end
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property int work_days
 *
 * @property User $user
 *
 */
class Calendar extends \common\models\ActiveRecord
{

    const TYPE_VACATION = 1;
    const TYPE_HOLIDAY = 2;

    const STATUS_AWAITING = 0;
    const STATUS_APPROVED = 1;
    const STATUS_DISAPPROVED = 2;

    public $username;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return Migration::TABLE_CALENDAR;
    }

    /**
     * @inheritdoc
     */
    public function setAttribute($name, $value)
    {
        if($name!='id') {
            if ($name == 'status') {
                if (!Yii::$app->user->isGuest) {
                    /**@var $user self */
                    $user = Yii::$app->user->identity;
                    if ($user->role < User::ROLE_PM) {
                        throw new ForbiddenHttpException();
                    }
                } else {
                    throw new ForbiddenHttpException();
                }

            }
        }
        self::resetWorkDays($this);
        parent::setAttribute($name, $value);
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            self::resetWorkDays($this);
            return true;
        } else {
            return false;
        }
    }


    /**
     * @return Profile
     */
    public function getUser(){

        return $this->hasOne(\frontend\models\User::className(),['id'=>'user_id']);

    }

    public static function statusList()
    {
        return [
            self::STATUS_AWAITING => [ 'text'=>'Awaiting', 'class'=>'label-warning'],
            self::STATUS_APPROVED => [ 'text'=>'Approved', 'class'=>'label-success'],
            self::STATUS_DISAPPROVED => [ 'text'=>'Disapproved', 'class'=>'label-danger'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type','user_id', 'title', 'description', 'start', 'created_at'], 'required','except'=>'search'],
            [['type','user_id', 'status'], 'integer','except'=>'search'],
            [['start', 'end', 'created_at', 'updated_at'], 'string','except'=>'search'],
            [['title'], 'string', 'max' => 100,'except'=>'search'],
            [['description'], 'string', 'max' => 255,'except'=>'search'],
            [['username','status'],'safe','on'=>'search']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'title' => 'Title',
            'description' => 'Description',
            'start' => 'Start',
            'end' => 'End',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @param array|self $holiday
     * @return int
     */
    public static function resetWorkDays($holiday)
    {
        return self::updateAll(['work_days' => null],
            ['and',
                ['type' => self::TYPE_VACATION],
                ['or',
                    ['and', ['<=', 'start',$holiday['start']], ['>=', 'end', $holiday['start']]],
                    ['and', ['<=', 'start', $holiday['end']], ['>=', 'end', $holiday['end']]]
                ]
            ]);
    }
}
