<?php

namespace common\models;

use console\migrations\Migration;
use Yii;

/**
 * This is the model class for table "profile".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $first_name
 * @property string $last_name
 * @property string $gender
 * @property string $position
 * @property int $vacation_days
 * @property string $employment_date
 * @property string $created_at
 * @property string $updated_at
 *
 *
 * @property string $fullName
 */
class Profile extends ActiveRecord
{
    const GENDER_FEMALE = 'F';
    const GENDER_MALE = 'M';

    const MEDIUM_FEMALE_AVATAR = '/default/medium-female.png';
    const MEDIUM_MALE_AVATAR = '/default/medium-male.png';
    const DEFAULT_AVATAR_SMALL = '/default/small-avatar.jpg';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return Migration::TABLE_PROFILE;
    }

    public function getFullName(){
        return trim($this->first_name.' '.$this->last_name);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'created_at'], 'required'],
            [['user_id','vacation_days'], 'integer'],
            [['created_at', 'updated_at','employment_date'], 'safe'],
            [['first_name', 'last_name', 'position'], 'string', 'max' => 30]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'position' => 'Position',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @param string $size.Default to 'medium'
     */
    public function getAvatar($size='medium'){
        $uploadedImageFolder  = Yii::getAlias('@frontend/web').'/uploads/'.$this->user_id.'/avatar';
        $avatarUrl = '';
        $avatar = new \stdClass();
        $avatar->name = 'todo';
        if($avatar && file_exists($uploadedImageFolder.'/'.$avatar->name)){
            switch($size){
                case "medium":
                    if(!file_exists($uploadedImageFolder.'/medium_'.$avatar->name)){
                        ini_set('memory_limit','256M');
                        \yii\imagine\Image::thumbnail('@frontend/web/uploads/'.$this->user_id.'/avatar/'.$avatar->name, 190, 190)
                            ->save($uploadedImageFolder.'/medium_'.$avatar->name, ['quality' => 80]);
                        if(file_exists($uploadedImageFolder.'/medium_'.$avatar->name)){
                            chmod($uploadedImageFolder.'/medium_'.$avatar->name,FILE_MODE);
                        }
                    }
                    $avatarUrl = '/uploads/'.$this->user_id.'/avatar/medium_'.$avatar->name;
                    break;
                case "small":
                    if(!file_exists($uploadedImageFolder.'/small_'.$avatar->name)){
                        ini_set('memory_limit','256M');
                        \yii\imagine\Image::thumbnail('@frontend/web/uploads/'.$this->user_id.'/avatar/'.$avatar->name, 60, 60)
                            ->save($uploadedImageFolder.'/small_'.$avatar->name, ['quality' => 80]);
                        if(file_exists($uploadedImageFolder.'/small_'.$avatar->name)){
                            chmod($uploadedImageFolder.'/small_'.$avatar->name,FILE_MODE);
                        }
                    }
                    $avatarUrl = '/uploads/'.$this->user_id.'/avatar/small_'.$avatar->name;
                    break;
                case "big":
                    $avatarUrl = '/uploads/'.$this->user_id.'/avatar/'.$avatar->name;
                    break;

            }
        }else{
            $avatarUrl = $this->getDefaultAvatar($size);
        }
        return $avatarUrl;
    }

    public function getDefaultAvatar($size){
        $defaultAvatarUrl = '';
        switch($size) {
            case "medium":
                $defaultAvatarUrl = $this->gender == self::GENDER_FEMALE ?
                    self::MEDIUM_FEMALE_AVATAR : self::MEDIUM_MALE_AVATAR;
                break;
            case "small":
                $defaultAvatarUrl = self::DEFAULT_AVATAR_SMALL;
                break;
        }
        return $defaultAvatarUrl;
    }
}
