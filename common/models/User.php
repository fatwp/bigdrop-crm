<?php
namespace common\models;

use console\migrations\Migration;
use Yii;
use yii\base\NotSupportedException;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\IdentityInterface;

use common\interfaces\IStatus;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property integer $role
 * @property string $email_confirmation_token
 * @property string $auth_key
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $password write-only password
 *
 * @property Profile $profile
 */
abstract class User extends ActiveRecord implements IdentityInterface,IStatus
{


    const ROLE_USER  = 1;
    const ROLE_DESIGNER  = 2;
    const ROLE_QA = 4;
    const ROLE_DEVELOPER = 8;
    const ROLE_PM = 16;
    const ROLE_ADMIN = 32;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return Migration::TABLE_USER;
    }

    public static function statusList()
    {
        return [
            self::STATUS_DELETED => [ 'text'=>'Deleted', 'class'=>'label-default', 'suffix'=>'default'],
            self::STATUS_PENDING => [ 'text'=>'Pending', 'class'=>'label-warning', 'suffix'=>'warning'],
            self::STATUS_ACTIVE => [ 'text'=>'Active', 'class'=>'label-success', 'suffix'=>'success'],
            self::STATUS_BANED => [ 'text'=>'Baned', 'class'=>'label-danger', 'suffix'=>'danger'],
        ];
    }

    /**
     * @return string
     */
    public function getRole()
    {
        switch($this->role){
            case self::ROLE_USER: return 'User';
            case self::ROLE_DESIGNER: return 'Designer';
            case self::ROLE_QA: return 'QA';
            case self::ROLE_DEVELOPER: return 'Developer';
            case self::ROLE_PM: return 'Project Manager';
            case self::ROLE_ADMIN: return 'Admin';
            default: return '';
        }
    }

    /**
     * @return string
     */
    public static function roleList()
    {
        $roles  = [
            self::ROLE_USER => 'User',
            self::ROLE_DESIGNER => 'Designer',
            self::ROLE_QA => 'QA',
            self::ROLE_DEVELOPER => 'Developer',
            self::ROLE_PM => 'Project Manager',
        ];

        if(!Yii::$app->user->isGuest){
            /**@var $user self*/
            $user = Yii::$app->user->identity;
            if($user->role >= self::ROLE_ADMIN){
                $roles[self::ROLE_ADMIN] = 'Admin';
            }
        }
        return $roles;
    }

    /**
     * @inheritdoc
     */
    public function setAttribute($name, $value)
    {
        if($name!='id'){
            if(Yii::$app->user->isGuest){
                throw new ForbiddenHttpException();
            }

            /**@var $user self*/
            $user = Yii::$app->user->identity;

            if($user->id != $this->id){
                if($user->role > User::ROLE_PM) {
                    if ($name == 'role') {

                        if ($user->role < self::ROLE_ADMIN && $value == self::ROLE_ADMIN) {
                            throw new ForbiddenHttpException();
                        }

                    } elseif ($name == 'status') {
                        if ($user->role < User::ROLE_PM) {
                            throw new ForbiddenHttpException();
                        }
                    }
                }else{
                    throw new ForbiddenHttpException();
                }

            }else{
                throw new ForbiddenHttpException();
            }
        }

        parent::setAttribute($name, $value);
    }


    /**
     * @return Profile
     */
    public function getProfile(){

        return $this->hasOne(Profile::className(),['user_id'=>'id']);

    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED, self::STATUS_BANED,self::STATUS_PENDING]],
            ['role', 'in', 'range' => [self::ROLE_ADMIN, self::ROLE_PM, self::ROLE_DEVELOPER,self::ROLE_QA,self::ROLE_DESIGNER,self::ROLE_USER]],
        ];
    }


    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
        ]);
    }

    /**
     * Finds user by email confirmation token token
     *
     * @param string $token email confirmation token
     * @return User|null
     */
    public static function findByEmailConfirmationToken($token)
    {
        if (!static::isTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'email_confirmation_token' => $token,
        ]);
    }

    /**
     * Finds out if  token is valid
     *
     * @param string $token
     * @return boolean
     */
    public static function isTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['tokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        return $timestamp + $expire >= time();
    }


    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Generates new email confirmation  token
     */
    public function generateEmailConfirmationToken()
    {
        $this->email_confirmation_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * Removes email confirmation reset token
     */
    public function removeEmailConfirmationToken()
    {
        $this->email_confirmation_token = null;
    }

    /**
     *@return boolean whether the message has been sent successfully
     */
    public function sendEmailConfirmation(){
        $this->generateEmailConfirmationToken();
        $this->save();
        return \Yii::$app->mailer->compose('email_confirmation_token_html', ['user' => $this])
            ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' robot'])
            ->setTo($this->email)
            ->setSubject('Registration on ' . \Yii::$app->name)
            ->send();
    }

    public function isPending()
    {
        return $this->status == self::STATUS_PENDING;
    }

    public function isDeleted()
    {
        return $this->status == self::STATUS_DELETED;
    }

    public function isSuspended()
    {
        return $this->status == self::STATUS_BANED;
    }
}
