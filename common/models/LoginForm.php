<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
abstract class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;

    protected $_user = false;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // email and password are both required
            [['username', 'password'], 'required'],

            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError('password', 'Incorrect username or password.');
                $this->addError('username', 'Incorrect username or password.');
            }elseif($user->isPending()) {
                $this->addError('username', 'Account with this email is awaiting moderation.');
            }elseif($user->isSuspended()){
                $this->addError('username', 'Account with this email has been suspended.');
            }elseif($user->isDeleted()){
                $this->addError('username', 'This account has been deactivated.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    abstract public function getUser();

}
