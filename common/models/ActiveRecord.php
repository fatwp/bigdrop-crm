<?php
/**
 * Created by PhpStorm.
 * User: buba
 * Date: 09.04.15
 * Time: 20:28
 */

namespace common\models;


use yii\helpers\VarDumper;

class ActiveRecord extends \yii\db\ActiveRecord{

    const DATE_FORMAT = "Y-m-d H:i:s";

    public function beforeValidate()
    {
        if($this->isNewRecord)
            $this->created_at = date(self::DATE_FORMAT);
        else
            $this->updated_at = date(self::DATE_FORMAT);

        return parent::beforeValidate();
    }


}