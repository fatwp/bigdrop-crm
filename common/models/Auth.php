<?php

namespace common\models;

use console\migrations\Migration;
use Yii;

/**
 * This is the model class for table "auth".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $source
 * @property string $source_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property \frontend\models\User $user
 */
class Auth extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return Migration::TABLE_AUTH;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'source', 'source_id', 'created_at'], 'required'],
            [['user_id'], 'integer'],
            ['source_id', 'match','pattern'=>'/^[a-z0-9A-Z_]+$/'],
            ['source', 'in','range'=>['google','facebook','twitter']],
            [['created_at', 'updated_at'], 'safe'],
            [['source', 'source_id'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'source' => 'Source',
            'source_id' => 'Source ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @param $client \yii\authclient\ClientInterface
     */
    public static function parseAttributes($client){
        $attributes = $client->getUserAttributes();
        $attributes['email']      = isset($attributes['email'])?$attributes['email']:null;
        $attributes['firstName'] = isset($attributes['first_name'])?$attributes['first_name']:null;
        $attributes['lastName']  = isset($attributes['last_name'])?$attributes['last_name']:null;
        $attributes['gender']     = isset($attributes['gender'])?\frontend\models\User::parseGender($attributes['gender']):null;
        switch($client->getId()){
            case 'facebook':

            // no break;
            case 'twitter':
                if(isset($attributes['name']) && !isset($attributes['firstName'],$attributes['lastName'])){
                    $attributes['name'] = explode(' ',$attributes['name'],2);
                    $attributes['firstName'] = trim($attributes['name'][0]);
                    if(2 === count($attributes['name'])){
                        $attributes['lastName']  = trim($attributes['name'][1]);
                    }else{
                        $attributes['lastName']  = null;
                    }
                }
            break;
            case 'google':

                if(isset($attributes['emails'],$attributes['emails'][0]) && !isset($attributes['email'])){
                    $attributes['email'] = isset($attributes['emails'][0]['value'])?$attributes['emails'][0]['value']:null;
                }

                if(isset($attributes['displayName']) && !isset($attributes['firstName'],$attributes['lastName'])){
                    $attributes['displayName'] = explode(' ',$attributes['displayName'],2);
                    $attributes['firstName'] = trim($attributes['displayName'][1]);
                    if(2 === count($attributes['displayName'])){
                        $attributes['lastName']  = trim($attributes['displayName'][1]);
                    }else{
                        $attributes['lastName']  = null;
                    }
                }

                break;

        }

        $attributes['username']   = $attributes['firstName']?strtolower($attributes['firstName']):null;

        return $attributes;
    }

    public function getUser(){
        return $this->hasOne(\frontend\models\User::className(),['id'=>'user_id']);
    }

}
