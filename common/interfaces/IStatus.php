<?php
namespace common\interfaces;

interface IStatus{

    const STATUS_DELETED    = 0;
    const STATUS_PENDING    = 3;
    const STATUS_ACTIVE     = 6;
    const STATUS_BANED      = 9;

    public static function statusList();

}